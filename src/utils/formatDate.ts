import moment from "moment";

export const formatDate = (unix: number, format = "L LTS") =>
  moment.unix(+unix || 0).format(format);
