import { config } from "@app/configs";
import { objFilters } from "./obj";

export const apiUrl = (action?: string, params?: Record<string, any>): string => {
  const baseUrl = (config?.apiUrl || window.location.origin).replace(/[\\/\\]*$/, "");
  const strAcction = action?.length ? action.replace(/^[\\/\\]*/, "") : '';
  const queryParams = params ? `?${new URLSearchParams(objFilters(params))}` : '';

  return `${baseUrl}/${strAcction}${queryParams}`;
};