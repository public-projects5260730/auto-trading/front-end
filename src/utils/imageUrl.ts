import { apiUrl } from "./apiUrl";

export const imageUrl = (url?: string): string => {
  if (!url?.length) {
    return '';
  }
  return apiUrl(`/storage/${url}`);
};
