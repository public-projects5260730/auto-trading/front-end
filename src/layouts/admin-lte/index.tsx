import { useMemo, useEffect } from "react";

import { useAppSelector, useWindowWidth } from "@app/hooks";
import { selectPageTitle, selectToggleSidebar } from "@app/store/header";

import "@app/styles/libs/font-awesome/css/font-awesome.min.css";
import "@app/styles/libs/fontawesome-free/css/all.min.css";
import "@app/styles/layout/adminlte.min.css";
import "@app/styles/app.scss";
import { Outlet } from "react-router-dom";
import {
  FooterComponent,
  HeaderComponent,
  PartnerSlideComponent,
  PopupSupportComponent,
  SideBarConatiner,
} from "@app/components";
import Tools from "./tools";
import { config } from "@app/configs";

const AdminLTE = () => {
  const windowWidth = useWindowWidth();
  const isToggle = useAppSelector(selectToggleSidebar);
  const pageTitle = useAppSelector(selectPageTitle);

  useEffect(() => {
    if (pageTitle?.length) {
      window.document.title = `${config?.siteName} | ${pageTitle}`;
    }
  }, [pageTitle]);

  const sidebarClass = useMemo(() => {
    if (isToggle) {
      return "sidebar-open";
    }
    return "sidebar-closed";
  }, [isToggle]);

  return (
    <div
      className={`${sidebarClass} layout-fixed${
        windowWidth < 768 ? " layout-footer-fixed" : ""
      }`}
    >
      <div className="wrapper">
        <HeaderComponent />
        <SideBarConatiner />

        <main>
          <Outlet />
          <PartnerSlideComponent />
        </main>
        <FooterComponent />
        <PopupSupportComponent />

        <Tools />
      </div>
    </div>
  );
};

export default AdminLTE;
