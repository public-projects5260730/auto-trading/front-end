import ReactTooltip from "react-tooltip";

const Tooltip = () => {
  return <ReactTooltip effect="solid" />;
};

export default Tooltip;
