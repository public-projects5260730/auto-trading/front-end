import { useAppSelector, useTrans } from "@app/hooks";
import { selectVisitor } from "@app/store/auth";
import { ISidebarNav } from "@app/types";
import SidebarAuth from "./SidebarAuth";
import SidebarNavItem from "./SidebarNavItem";

const SidebarNav = () => {
  const trans = useTrans();

  const visitor = useAppSelector(selectVisitor);

  const navList: ISidebarNav[] = [
    {
      title: trans("main_menu"),
      session: "main",
      // icon: 'fas fa-tachometer-alt',
      children: [
        {
          title: trans("home"),
          link: "/",
          session: "home",
          icon: "fa fa-home",
          // visible: !!visitor?.id,
        },
        {
          title: trans("accounts"),
          link: "/accounts",
          session: "accounts",
          icon: "fas fa-id-card",
          // visible: !!visitor?.permissions?.canManagementAccounts,
        },
        {
          title: trans("groups"),
          link: "/groups",
          session: "groups",
          icon: "fas fa-users",
          // visible: !!visitor?.permissions?.canManagementGroups,
        },
        {
          title: trans("circles"),
          link: "/circles",
          session: "circles",
          icon: "far fa-circle",
          // visible: !!visitor?.permissions?.canCircleGroups,
        },
      ],
    },
    {
      title: trans("FTMO"),
      session: "ftmo",
      children: [
        {
          title: trans("phrase_1_list"),
          link: "/phases/1",
          session: "phrases-1",
          icon: "fa fa-address-book",
        },
        {
          title: trans("phrase_2_list"),
          link: "/phases/2",
          session: "phrases-2",
          icon: "fa fa-address-book",
        },
        {
          title: trans("phrase_3_list"),
          link: "/phases/3",
          session: "phrases-3",
          icon: "fa fa-address-book",
        },
        {
          title: trans("phrase_4_list"),
          link: "/phases/4",
          session: "phrases-4",
          icon: "fa fa-address-book",
        },
      ],
      visible: !!visitor?.id,
    },
    {
      title: trans("MFF"),
      session: "mff",
      children: [
        {
          title: trans("phrase_1_list"),
          link: "/phases/1",
          session: "phrases-1",
          icon: "fa fa-address-book",
        },
        {
          title: trans("phrase_2_list"),
          link: "/phases/2",
          session: "phrases-2",
          icon: "fa fa-address-book",
        },
        {
          title: trans("phrase_3_list"),
          link: "/phases/3",
          session: "phrases-3",
          icon: "fa fa-address-book",
        },
        {
          title: trans("phrase_4_list"),
          link: "/phases/4",
          session: "phrases-4",
          icon: "fa fa-address-book",
        },
      ],
      visible: !!visitor?.id,
    },
  ];

  return (
    <nav className="mt-2">
      <ul className="nav nav-pills nav-sidebar flex-column nav-flat">
        {navList.map((nav, i) => (
          <SidebarNavItem nav={nav} key={i} />
        ))}
        <SidebarAuth />
      </ul>
    </nav>
  );
};

export default SidebarNav;
