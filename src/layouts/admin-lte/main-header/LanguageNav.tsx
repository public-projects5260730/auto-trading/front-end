import { useAppDispatch, useAppSelector } from "@app/hooks";
import { languageActions, selectLanguage } from "@app/store/language";
import { useEffect } from "react";
import { useTranslation } from "react-i18next";
import { Dropdown } from "react-bootstrap";
import { SLanguages } from "@app/store/language/languages";

const LanguageNav = ({
  isShowLanguageName,
}: {
  isShowLanguageName?: boolean;
}) => {
  const language = useAppSelector(selectLanguage);
  const dispatch = useAppDispatch();

  const handleChangeLanguage = (state: { code: string; title: string }) => {
    dispatch(languageActions.change(state));
  };

  const { i18n } = useTranslation();
  const { t: trans } = useTranslation();

  useEffect(() => {
    i18n.changeLanguage(language?.code || "en");
  }, [language?.code, i18n]);

  return (
    <Dropdown className="nav-item languages-menu">
      <Dropdown.Toggle
        as="a"
        className="nav-link languages-menu__toggle"
        href="#"
      >
        {/* <p>{trans("languages")}</p> */}
        <img
          src={`/assets/images/languages/${language.code}.png`}
          alt={language.title}
          width={24}
          height={14}
        />
        {isShowLanguageName && language.title}
      </Dropdown.Toggle>
      <Dropdown.Menu className="dropdown-menu-right p-0">
        {SLanguages.map(
          (SLanguage, index) =>
            SLanguage.code !== language.code && (
              <Dropdown.Item
                onClick={() => {
                  handleChangeLanguage(SLanguage);
                }}
                key={index}
                className="rounded"
              >
                <div className="d-flex align-items-center">
                  <img
                    src={`/assets/images/languages/${SLanguage.code}.png`}
                    alt={SLanguage.title}
                    className="mr-2"
                    height="12"
                  />
                  <div className="text-muted text-sm">{SLanguage.title}</div>
                </div>
              </Dropdown.Item>
            )
        )}
      </Dropdown.Menu>
    </Dropdown>
  );
};
export default LanguageNav;
