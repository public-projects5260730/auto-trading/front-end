import React, { MouseEvent } from "react";
import { Link, useLocation } from "react-router-dom";
import LanguageNav from "@app/layouts/admin-lte/main-header/LanguageNav";
import HeaderItemComponent from "./HeaderItem.component";
import { useAppDispatch, useAppSelector, useTrans } from "@app/hooks";
import Avatar from "react-avatar";
import { selectVisitor } from "@app/store/auth";
import { imgLinktoServer } from "@app/utils";
import { headerActions, selectToggleSidebar } from "@app/store/header";

const HeaderComponent = () => {
  const trans = useTrans();
  const location = useLocation();
  const pathname = location.pathname;
  const visitor = useAppSelector(selectVisitor);
  const dispatch = useAppDispatch();
  const isToggle = useAppSelector(selectToggleSidebar);

  const sidebarToggle = (e: MouseEvent) => {
    e.preventDefault();
    dispatch(headerActions.update({ isToggleSidebar: !isToggle }));
  };

  return (
    <header id="header">
      <div className="header-grid__block">
        <div className="header__logo">
          <Link to="/" className="header__logo-link">
            <img
              src="/assets/logo/logo_1.svg"
              alt="TimeTrading_logo"
              className="header__logo-img"
            />
          </Link>

          <div className="header__logo-menu" onClick={sidebarToggle}>
            <img src="/assets/images/menu.svg" alt="menu button" />
          </div>
        </div>

        <div className="header__page">
          <div className="container">
            <nav className="navbar navbar-expand-lg">
              <ul className="navbar-nav">
                <HeaderItemComponent
                  linkPage="/"
                  pageName="home"
                  pathname={pathname}
                />
                <HeaderItemComponent
                  linkPage="/introduce"
                  pageName="introduce"
                  pathname={pathname}
                />
                <HeaderItemComponent
                  linkPage="/auto-trading"
                  pageName="auto_trading"
                  pathname={pathname}
                />
                <HeaderItemComponent
                  linkPage="/products"
                  pageName="products"
                  pathname={pathname}
                />
                <HeaderItemComponent
                  linkPage="/affiliate"
                  pageName="affiliate"
                  pathname={pathname}
                />
                <HeaderItemComponent
                  linkPage="/faqs"
                  pageName="faqs"
                  pathname={pathname}
                />
                <HeaderItemComponent
                  linkPage="/contacts-us"
                  pageName="contacts_us"
                  pathname={pathname}
                />
              </ul>
            </nav>
          </div>
        </div>

        <div className="header__right">
          {/* <div className="header__search-input">
          <input type="text" placeholder="Search...."></input>
          <FontAwesomeIcon
            icon={faMagnifyingGlass}
            className="header__search-icon"
          />
        </div> */}
          {!visitor ? (
            <ul className="header__right-list">
              <li className="header__right-list-item">
                <Link to="/login" className="header__right-list-link">
                  {trans("sign_in")}
                </Link>
              </li>
              <li className="header__right-list-item">
                <Link to="/register" className="header__right-list-link">
                  {trans("sign_up")}
                </Link>
              </li>
            </ul>
          ) : (
            <div className="header__user">
              <div className="header__user-avatar">
                <Avatar
                  size="52"
                  style={{ height: "auto" }}
                  src={`${imgLinktoServer}${visitor.avatar}`}
                  alt="user avatar"
                />
              </div>
              <ul className="header__user-list">
                <li className="header__user-list-item">
                  <Link to="/user" className="header__user-list-link">
                    <img src="/assets/images/profile.svg" alt="user" />
                    <p>{trans("profile")}</p>
                  </Link>
                </li>
                <li className="header__user-list-item">
                  <Link to="/user/products" className="header__user-list-link">
                    <img src="/assets/images/product.svg" alt="cubes" />
                    <p>{trans("products")}</p>
                  </Link>
                </li>
                <li className="header__user-list-item">
                  <Link to="/logout" className="header__user-list-link">
                    <img src="/assets/images/logout.svg" alt="cubes" />
                    <p>{trans("log_out")}</p>
                  </Link>
                </li>
              </ul>
            </div>
          )}
          <LanguageNav />
        </div>
      </div>
    </header>
  );
};

export default HeaderComponent;
