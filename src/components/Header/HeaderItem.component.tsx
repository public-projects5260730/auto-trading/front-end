import React from "react";
import { useTrans } from "@app/hooks";
import { Link } from "react-router-dom";

const HeaderItemComponent = ({
  pageName,
  linkPage,
  pathname,
}: {
  pageName: string;
  linkPage: string;
  pathname: string;
}) => {
  const trans = useTrans();

  return (
    <li
      className={`nav-item ${
        linkPage === pathname || pathname.includes(`/${pageName}/`)
          ? "active"
          : ""
      }`}
    >
      <Link to={linkPage} className="nav-link">
        {trans(pageName)}
      </Link>
    </li>
  );
};

export default HeaderItemComponent;
