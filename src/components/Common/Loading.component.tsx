import React from "react";
import { useTrans } from "@app/hooks";

const LoadingComponent = () => {
  const trans = useTrans();

  return (
    <div id="loading">
      <div className="container">
        <div className="loading__content">
          <i className="fas fa-circle-o-notch fa-2x fa-spin loading__content-icon"></i>
          <p className="loading__content-text">{trans("loading")}</p>
        </div>
      </div>
    </div>
  );
};

export default LoadingComponent;
