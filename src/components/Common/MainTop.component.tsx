import { useTrans } from "@app/hooks";
import React from "react";

const MainTopComponent = () => {
  const trans = useTrans();

  return (
    <div className="main__top">
      <div className="main__top-background">
        <div className="container">
          <div className="row">
            <div className="col-md-5 col-sm-12">
              <div className="main__top-text">
                <h1>{trans("auto_trading")}</h1>
                <h3>{trans("header.so_simple")}</h3>
                <p>{trans("header.access_to_market")}</p>
              </div>
            </div>
            <div className="col-md-7 col-sm-12">
              <img
                src="/assets/images/robot.png"
                alt="robot"
                className="main__top-img"
              />
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default MainTopComponent;
