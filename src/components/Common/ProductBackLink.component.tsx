import React from "react";
import { useTrans } from "@app/hooks";
import { Link } from "react-router-dom";

const ProductBackLinkComponent = () => {
  const trans = useTrans();

  return (
    <div className="product__back-link">
      <Link to="/products" className="product__back-link-link">
        <img src="/assets/images/Polygon 2.svg" alt="" />
        <p>{trans("back_to_product")}</p>
      </Link>
    </div>
  );
};

export default ProductBackLinkComponent;
