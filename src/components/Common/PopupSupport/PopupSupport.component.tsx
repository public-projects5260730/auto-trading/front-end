import React, { useState } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faTimes } from "@fortawesome/free-solid-svg-icons";
import { useAppSelector, useTrans } from "@app/hooks";
import { FormProvider, useForm } from "react-hook-form";
import { IMessage } from "@app/types";
import { messageApi } from "@app/apis";
import Form from "@app/components/Form";
import { selectVisitor } from "@app/store/auth";
import PopupSupportSmallComponent from "./PopupSupportSmall.component";

const PopupSupportComponent = () => {
  const trans = useTrans();
  const visitor = useAppSelector(selectVisitor);

  const [sendError, setSendError] = useState("");
  const [isSubmitting, setIsSubmitting] = useState(false);
  const [sendMessageSuccess, setSendMessageSuccess] = useState(false);
  const [isShow, setIsShow] = useState(false);
  const [showPopup, setShowPopup] = useState(true);

  const methods = useForm<IMessage>({
    defaultValues: {
      name: visitor?.name || "",
      email: visitor?.email || "",
    },
  });
  const { setError, handleSubmit, reset } = methods;

  const onSubmitSend = handleSubmit((formData: IMessage) => {
    if (isSubmitting) {
      return;
    }
    setIsSubmitting(true);

    messageApi
      .sendMessage({ ...formData })
      .then(({ errors, error, successfully }) => {
        setIsSubmitting(false);
        if (error) {
          setSendError(error);
        }
        if (errors) {
          Object.entries(errors).map(([field, message]) => {
            return setError(field as keyof IMessage, {
              type: "manual",
              message,
            });
          });
        }

        if (successfully) {
          setSendMessageSuccess(true);
          reset({
            message: "",
            title: "",
          });
        }
      });
  });

  return (
    <div className={`popup ${!showPopup ? "notshow" : ""}`}>
      {!isShow ? (
        <PopupSupportSmallComponent
          isShow={isShow}
          setIsShow={setIsShow}
          setShowPopup={setShowPopup}
          showPopup={showPopup}
        />
      ) : (
        <div className="popup__lg">
          <div className="popup__lg-text">
            <h4>{trans("popup.leave_a_message")}</h4>
            <FontAwesomeIcon
              icon={faTimes}
              className="popup__lg-text-icon"
              onClick={() => setIsShow(!isShow)}
            />
          </div>
          <div className="popup__lg-chat">
            <div className="popup__lg-chat-logo">
              <img src="/assets/images/Group 114.svg" alt="TimeTrading_logo" />
              <p>{trans("popup.leave_a_message_email")}</p>
            </div>

            {!sendMessageSuccess ? (
              <FormProvider {...methods}>
                <form onSubmit={onSubmitSend}>
                  {sendError?.length > 0 && (
                    <div className="callout callout-danger text-danger p-2 small">
                      {sendError}
                    </div>
                  )}

                  <div className="popup__lg-chat-message">
                    {!visitor && (
                      <>
                        <Form.Row
                          name="your_name"
                          isRequired
                          className="col-12"
                        >
                          <Form.InputGroup name="name">
                            <Form.InputBox
                              name="name"
                              disabled={isSubmitting}
                              isRequired
                              className="popup__lg-chat-message-text"
                            />
                          </Form.InputGroup>
                        </Form.Row>
                        <Form.Row
                          name="your_email"
                          isRequired
                          className="col-12"
                        >
                          <Form.InputGroup name="email">
                            <Form.InputBox
                              type="email"
                              name="email"
                              disabled={isSubmitting}
                              isRequired
                              className="popup__lg-chat-message-text"
                            />
                          </Form.InputGroup>
                        </Form.Row>
                      </>
                    )}
                    <Form.Row name="subject" className="col-12">
                      <Form.InputGroup name="title">
                        <Form.InputBox
                          name="title"
                          disabled={isSubmitting}
                          className="popup__lg-chat-message-text"
                        />
                      </Form.InputGroup>
                    </Form.Row>
                    <Form.Row name="your_message" className="col-12">
                      <Form.TextAreaBox
                        name="message"
                        label={
                          trans("popup.type_your_message").toString() || ""
                        }
                        className="popup__lg-chat-message-textarea"
                      />
                    </Form.Row>
                  </div>

                  <div className="popup__lg-send">
                    <Form.Button
                      isSubmitting={isSubmitting}
                      label={trans("send")}
                      className="popup__lg-send-btn"
                    />
                  </div>
                </form>
              </FormProvider>
            ) : (
              <div className="popup__success">
                <div className="popup__success-message">
                  <i className="fa fa-check-circle popup__success-icon"></i>
                  <h6 className="">{trans("send_message_successfull")}</h6>
                </div>

                <div
                  className="popup__success-link"
                  onClick={() => setSendMessageSuccess(!sendMessageSuccess)}
                >
                  <img src="/assets/images/Polygon 2.svg" alt="" />
                  <p>{trans("send_another_message")}</p>
                </div>
              </div>
            )}
          </div>
        </div>
      )}
    </div>
  );
};

export default PopupSupportComponent;
