import React, { Dispatch, SetStateAction } from "react";
import { useTrans } from "@app/hooks";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faTimes } from "@fortawesome/free-solid-svg-icons";

const PopupSupportSmallComponent = ({
  isShow,
  showPopup,
  setIsShow,
  setShowPopup,
}: {
  isShow: boolean;
  showPopup: boolean;
  setIsShow: Dispatch<SetStateAction<boolean>>;
  setShowPopup: Dispatch<SetStateAction<boolean>>;
}) => {
  const trans = useTrans();

  return (
    <div className="popup__small">
      <div className="popup__small-full">
        <div className="row">
          <div className="col-3">
            <div className="popup__small-img">
              <img src="/assets/images/Group 124.svg" alt="Comments icon" />
            </div>
          </div>
          <div className="col-9">
            <div className="popup__small-right">
              <div
                className="popup__small-right-text"
                onClick={() => setIsShow(!isShow)}
              >
                <h4>{trans("popup.have_questions")}</h4>
                <p>{trans("popup.click_to_chat")}</p>
              </div>
              <FontAwesomeIcon
                icon={faTimes}
                className="popup__small-right-icon"
                onClick={() => setShowPopup(!showPopup)}
              />
            </div>
          </div>
        </div>
      </div>
      <div className="popup__small-short" onClick={() => setIsShow(!isShow)}>
        <img src="/assets/images/Group 124.svg" alt="Comments icon" />
        <FontAwesomeIcon
          icon={faTimes}
          className="popup__small-short-icon"
          onClick={() => setShowPopup(!showPopup)}
        />
      </div>
    </div>
  );
};

export default PopupSupportSmallComponent;
