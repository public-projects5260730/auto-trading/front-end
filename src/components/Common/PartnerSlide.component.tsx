import { useTrans } from "@app/hooks";
import { Swiper, SwiperSlide } from "swiper/react";
import { Navigation, Autoplay } from "swiper";
import { Link } from "react-router-dom";

const PartnerSlideComponent = () => {
  const trans = useTrans();

  return (
    <div className="home__partner">
      <div className="home__partner-background">
        <div className="container">
          <h3>{trans("home_content.our_partner")}</h3>
          <div className="home-partner__slide">
            <Swiper
              grabCursor={true}
              navigation={true}
              modules={[Navigation, Autoplay]}
              spaceBetween={10}
              className="home-partner__slide-swiper"
              loop={true}
              slidesPerView={4}
            >
              <SwiperSlide className="home-partner__slide-swiper-item">
                <div className="home-partner__silde-img">
                  <Link to={" "}>
                    <img src="/assets/images/gsdr.svg" alt="IC Market" />
                  </Link>
                </div>
              </SwiperSlide>
              <SwiperSlide className="home-partner__slide-swiper-item">
                <div className="home-partner__silde-img">
                  <Link to={" "}>
                    <img src="/assets/images/gse.svg" alt="Tickmill" />
                  </Link>
                </div>
              </SwiperSlide>
              <SwiperSlide className="home-partner__slide-swiper-item">
                <div className="home-partner__silde-img">
                  <Link to={" "}>
                    <img src="/assets/images/ghsdr.svg" alt="FXCE" />
                  </Link>
                </div>
              </SwiperSlide>
              <SwiperSlide className="home-partner__slide-swiper-item">
                <div className="home-partner__silde-img">
                  <Link to={" "}>
                    <img src="/assets/images/Layer 1581.svg" alt="Exness" />
                  </Link>
                </div>
              </SwiperSlide>
              <SwiperSlide className="home-partner__slide-swiper-item">
                <div className="home-partner__silde-img">
                  <Link to={" "}>
                    <img src="/assets/images/gsdr.svg" alt="IC Market" />
                  </Link>
                </div>
              </SwiperSlide>
              <SwiperSlide className="home-partner__slide-swiper-item">
                <div className="home-partner__silde-img">
                  <Link to={" "}>
                    <img src="/assets/images/gse.svg" alt="Tickmill" />
                  </Link>
                </div>
              </SwiperSlide>
              <SwiperSlide className="home-partner__slide-swiper-item">
                <div className="home-partner__silde-img">
                  <Link to={" "}>
                    <img src="/assets/images/ghsdr.svg" alt="FXCE" />
                  </Link>
                </div>
              </SwiperSlide>
              <SwiperSlide className="home-partner__slide-swiper-item">
                <div className="home-partner__silde-img">
                  <Link to={" "}>
                    <img src="/assets/images/Layer 1581.svg" alt="Exness" />
                  </Link>
                </div>
              </SwiperSlide>
              <SwiperSlide className="home-partner__slide-swiper-item">
                <div className="home-partner__silde-img">
                  <Link to={" "}>
                    <img src="/assets/images/gsdr.svg" alt="IC Market" />
                  </Link>
                </div>
              </SwiperSlide>
              <SwiperSlide className="home-partner__slide-swiper-item">
                <div className="home-partner__silde-img">
                  <Link to={" "}>
                    <img src="/assets/images/gse.svg" alt="Tickmill" />
                  </Link>
                </div>
              </SwiperSlide>
              <SwiperSlide className="home-partner__slide-swiper-item">
                <div className="home-partner__silde-img">
                  <Link to={" "}>
                    <img src="/assets/images/ghsdr.svg" alt="FXCE" />
                  </Link>
                </div>
              </SwiperSlide>
              <SwiperSlide className="home-partner__slide-swiper-item">
                <div className="home-partner__silde-img">
                  <Link to={" "}>
                    <img src="/assets/images/Layer 1581.svg" alt="Exness" />
                  </Link>
                </div>
              </SwiperSlide>
            </Swiper>
          </div>
        </div>
      </div>
    </div>
  );
};

export default PartnerSlideComponent;
