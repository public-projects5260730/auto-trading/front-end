import React from "react";
import { useTrans } from "@app/hooks";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faFaceFrown } from "@fortawesome/free-solid-svg-icons";

const NoDataComponent = () => {
  const trans = useTrans();

  return (
    <div id="no-data">
      <div className="no-data__content">
        <FontAwesomeIcon icon={faFaceFrown} className="no-data__content-icon" />
        <p>{trans("no-data")}</p>
      </div>
    </div>
  );
};

export default NoDataComponent;
