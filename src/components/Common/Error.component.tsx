import React from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faExclamationCircle } from "@fortawesome/free-solid-svg-icons";
import { useTrans } from "@app/hooks";

const ErrorComponent = ({ error }: { error: string | undefined }) => {
  const trans = useTrans();

  return (
    <div id="error">
      <div className="error__content">
        <div className="error__content-icon">
          <FontAwesomeIcon
            icon={faExclamationCircle}
            className="error__content-icon-icon"
          />
        </div>
        {error ? (
          <p>{error}</p>
        ) : (
          <div>
            <p>{trans("something_wrong")}</p>
            <p>{trans("fix_or_update")}</p>
            <p>{trans("come_back")}</p>
          </div>
        )}
      </div>
    </div>
  );
};

export default ErrorComponent;
