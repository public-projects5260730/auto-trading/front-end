import React, { Dispatch, SetStateAction } from "react";
import { useTrans } from "@app/hooks";
import { Link } from "react-router-dom";

const SidebarTabItemComponent = ({
  linkPage,
  pageName,
  imgLink,
  tabActive,
  setTabActive,
}: {
  linkPage: string;
  pageName: string;
  imgLink?: string;
  tabActive: string;
  setTabActive: Dispatch<SetStateAction<string>>;
}) => {
  const trans = useTrans();

  return (
    <li
      className={`sidebar-user__info-list-item ${
        tabActive === pageName || tabActive === linkPage ? "active" : ""
      }`}
      onClick={() => setTabActive(linkPage)}
    >
      <Link to={linkPage} className="sidebar-user__info-list-link">
        {imgLink ? (
          <>
            <img src={imgLink} alt="" />
            <p>{trans(pageName)}</p>
          </>
        ) : (
          <>{trans(pageName)}</>
        )}
      </Link>
    </li>
  );
};

export default SidebarTabItemComponent;
