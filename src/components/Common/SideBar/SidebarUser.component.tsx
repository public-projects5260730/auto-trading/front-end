import React, { useEffect, useState, Dispatch, SetStateAction } from "react";
import { useAppSelector, useTrans } from "@app/hooks";
import SidebarTabItemComponent from "./SidebarTabItem.component";
import { Link, useLocation } from "react-router-dom";
import { selectVisitor } from "@app/store/auth";
import Avatar from "react-avatar";
import { imgLinktoServer } from "@app/utils";

const SidebarUserComponent = ({
  tabActive,
  setTabActive,
}: {
  tabActive: string;
  setTabActive: Dispatch<SetStateAction<string>>;
}) => {
  const trans = useTrans();
  const visitor = useAppSelector(selectVisitor);
  const [isShow, setIsShow] = useState(false);
  const { pathname } = useLocation();

  useEffect(() => {
    if (pathname.includes("user")) {
      setIsShow(true);
      return;
    }

    setIsShow(false);
  }, [pathname]);

  return (
    <div className="sidebar__user">
      {!visitor ? (
        <div className="sidebar-user__login">
          <Link to="/login" className="sidebar-user__login-list-link">
            <button className="sidebar-user__login-list-btn login">
              {trans("sign_in")}
            </button>
          </Link>
          <Link to="/register" className="sidebar-user__login-list-link">
            <button className="sidebar-user__login-list-btn register">
              {trans("sign_up")}
            </button>
          </Link>
        </div>
      ) : (
        <div className="sidebar-user__info">
          <div
            className="sidebar-user__info-user"
            onClick={() => setIsShow(!isShow)}
          >
            <div className="sidebar-user__info-avatar-container">
              <Avatar
                size="50"
                style={{ height: "auto" }}
                src={`${imgLinktoServer}${visitor.avatar}`}
                alt="user avatar"
                className="sidebar-user__info-avatar"
              />
              <p>{visitor.name}</p>
            </div>

            <div className="sidebar-user__info-user-img">
              <img src="/assets/images/down.svg" alt="down button" />
            </div>
          </div>

          <ul className={`sidebar-user__info-list ${isShow ? "show" : ""}`}>
            <SidebarTabItemComponent
              linkPage="/user"
              pageName="profile"
              imgLink="/assets/images/profile.svg"
              tabActive={tabActive}
              setTabActive={setTabActive}
            />
            <SidebarTabItemComponent
              linkPage="/user/products"
              pageName="products"
              imgLink="/assets/images/product.svg"
              tabActive={tabActive}
              setTabActive={setTabActive}
            />
            <SidebarTabItemComponent
              linkPage="/logout"
              pageName="log_out"
              imgLink="/assets/images/logout.svg"
              tabActive={tabActive}
              setTabActive={setTabActive}
            />
          </ul>
        </div>
      )}
    </div>
  );
};

export default SidebarUserComponent;
