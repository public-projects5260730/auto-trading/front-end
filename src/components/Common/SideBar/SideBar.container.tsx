import React, { useMemo, useEffect, useRef, useState } from "react";
import { useAppDispatch, useAppSelector, useWindowWidth } from "@app/hooks";
import { useLocation } from "react-router-dom";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faXmark } from "@fortawesome/free-solid-svg-icons";
import { headerActions, selectToggleSidebar } from "@app/store/header";
import LanguageNav from "@app/layouts/admin-lte/main-header/LanguageNav";
import SidebarTabItemComponent from "./SidebarTabItem.component";
import SidebarUserComponent from "./SidebarUser.component";

const SideBarConatiner = () => {
  const isToggle = useAppSelector(selectToggleSidebar);
  const dispatch = useAppDispatch();
  const { pathname } = useLocation();
  const sidebarRef = useRef<any>(null);
  const windowWidth = useWindowWidth();
  const [tabActive, setTabActive] = useState("home");

  const showClass = useMemo(
    () => (isToggle && windowWidth < 992 ? "show" : ""),

    // eslint-disable-next-line react-hooks/exhaustive-deps
    [isToggle]
  );

  useEffect(() => {
    if (windowWidth >= 992 || !isToggle) {
      return;
    }

    dispatch(headerActions.update({ isToggleSidebar: false }));

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [pathname]);

  const handleEvent = (e: MouseEvent) => {
    if (windowWidth >= 992 || !isToggle) {
      return;
    }
    if (!sidebarRef?.current?.contains(e?.target)) {
      dispatch(headerActions.update({ isToggleSidebar: false }));
    }
  };

  useEffect(() => {
    document.addEventListener("mousedown", handleEvent);

    return () => {
      window.removeEventListener("mousedown", handleEvent);
    };

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [isToggle]);

  return (
    <div>
      <aside id="sidebar" ref={sidebarRef} className={showClass}>
        <div className="sidebar__icon">
          <FontAwesomeIcon
            icon={faXmark}
            onClick={() =>
              dispatch(headerActions.update({ isToggleSidebar: false }))
            }
            className="sidebar__icon-xmark"
          />
        </div>

        <SidebarUserComponent
          tabActive={tabActive}
          setTabActive={setTabActive}
        />

        <div>
          <ul className="sidebar-user__info-list">
            <SidebarTabItemComponent
              linkPage="/"
              pageName="home"
              tabActive={tabActive}
              setTabActive={setTabActive}
            />
            <SidebarTabItemComponent
              linkPage="/introduce"
              pageName="introduce"
              tabActive={tabActive}
              setTabActive={setTabActive}
            />
            <SidebarTabItemComponent
              linkPage="/auto-trading"
              pageName="auto_trading"
              tabActive={tabActive}
              setTabActive={setTabActive}
            />
            <SidebarTabItemComponent
              linkPage="/products"
              pageName="products"
              tabActive={tabActive}
              setTabActive={setTabActive}
            />
            <SidebarTabItemComponent
              linkPage="/affiliate"
              pageName="affiliate"
              tabActive={tabActive}
              setTabActive={setTabActive}
            />
            <SidebarTabItemComponent
              linkPage="/faqs"
              pageName="faqs"
              tabActive={tabActive}
              setTabActive={setTabActive}
            />
            <SidebarTabItemComponent
              linkPage="/contacts-us"
              pageName="contacts_us"
              tabActive={tabActive}
              setTabActive={setTabActive}
            />
            <li className="sidebar-user__info-list-item">
              <LanguageNav isShowLanguageName={true} />
            </li>
          </ul>
        </div>
      </aside>
    </div>
  );
};

export default SideBarConatiner;
