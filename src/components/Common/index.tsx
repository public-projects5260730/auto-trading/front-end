export * from "./Avatar";
export * from "./Pagination";
export * from "./PopupSupport";
export * from "./SideBar";
export { default as PartnerSlideComponent } from "./PartnerSlide.component";
export { default as MainTopComponent } from "./MainTop.component";
export { default as LoadingComponent } from "./Loading.component";
export { default as ErrorComponent } from "./Error.component";
export { default as NoDataComponent } from "./NoData.component";
export { default as ComingSoonComponent } from "./ComingSoon.component";
export { default as ProductBackLinkComponent } from "./ProductBackLink.component";
