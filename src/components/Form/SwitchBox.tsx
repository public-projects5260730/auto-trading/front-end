import { useFormContext } from "react-hook-form";
import { IFormInputProps } from "@app/types";
import { useTrans } from "@app/hooks";
import { phraseByName } from "@app/utils";

const SwitchBox = ({
  name: _name,
  label,
  rules,
  ...props
}: IFormInputProps) => {
  const trans = useTrans();
  const { register } = useFormContext();

  const name = _name || "";
  const _label = label ? label : trans(phraseByName(name));

  return (
    <div className="row form-group">
      <label className="col-md-4 col-form-label text-md-right"></label>
      <div className="col-md-8 input-group custom-control custom-switch">
        <input
          type="checkbox"
          className="custom-control-input"
          id={`ctrl-${name}`}
          {...register(name, rules)}
          {...props}
        />
        <label className="custom-control-label" htmlFor={`ctrl-${name}`}>
          {_label}
        </label>
      </div>
    </div>
  );
};

export default SwitchBox;
