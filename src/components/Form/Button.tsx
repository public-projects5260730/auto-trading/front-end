import { IFormButtonProps } from "@app/types";
import { useTrans } from "@app/hooks";

const Button = ({ label, isSubmitting, icon, ...props }: IFormButtonProps) => {
  const trans = useTrans();

  const _label = label ? label : trans("submit");

  return (
    <button type="submit" className="btn btn-primary" {...props}>
      {(!!isSubmitting && (
        <i className="fa fa-circle-o-notch fa-spin mr-2"></i>
      )) ||
        (icon && icon)}
      <p>{_label}</p>
    </button>
  );
};

export default Button;
