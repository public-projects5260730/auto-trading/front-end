import React from "react";
import { useTrans } from "@app/hooks";
import { formatNumber } from "@app/utils";

type moreInfoType = {
  textInfo?: string;
  widthPercentInfo: number;
  valueInfo?: number;
};

const ProductChartInfoItemComponent = ({
  text,
  value,
}: {
  text: string;
  value: number;
  infoBlue?: moreInfoType;
  infoOrange?: moreInfoType;
}) => {
  const trans = useTrans();

  return (
    <div className="home-chart__right-info">
      <div className="row">
        <div className="col-6">
          <div className="home-chart__right-text">{trans(text)}</div>
        </div>
        <div className="col-6">
          <div className="home-chart__right-value">
            {formatNumber(value)} USD
          </div>
        </div>
        {/* <div className="col-5">
          <div className="home-chart__right-retangle">
            {infoBlue && (
              <div
                className="home-chart__right-retangle-1"
                style={{
                  width: `${
                    infoBlue?.widthPercentInfo
                      ? `${infoBlue?.widthPercentInfo}%`
                      : "2%"
                  }`,
                  backgroundColor: `${
                    (!infoBlue?.widthPercentInfo ||
                      infoBlue?.widthPercentInfo === 0) &&
                    "#7DD1F4"
                  }`,
                }}
              ></div>
            )}
            {infoOrange && (
              <div
                className="home-chart__right-retangle-2"
                style={{
                  width: `${
                    infoOrange?.widthPercentInfo
                      ? `${infoOrange?.widthPercentInfo}%`
                      : "0%"
                  }`,
                }}
              ></div>
            )}
          </div>
        </div> */}
      </div>

      {/* {((infoBlue && infoBlue.textInfo && infoBlue.valueInfo) ||
        (infoOrange && infoOrange.textInfo && infoOrange.valueInfo)) && (
        <div className="home-chart__right-more">
          {infoBlue && (
            <p className="home-chart__right-blue">
              {infoBlue?.textInfo}:{infoBlue?.valueInfo}
            </p>
          )}

          {infoOrange && (
            <p className="home-chart__right-organge">
              {infoOrange?.textInfo}:{infoOrange?.valueInfo}
            </p>
          )}
        </div>
      )} */}
    </div>
  );
};

export default ProductChartInfoItemComponent;
