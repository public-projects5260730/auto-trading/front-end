import React, { useEffect, useState } from "react";
import { useWindowWidth } from "@app/hooks";
import {
  PolarAngleAxis,
  PolarGrid,
  PolarRadiusAxis,
  Radar,
  RadarChart,
  Tooltip,
} from "recharts";

type RadarChartType = {
  title: string;
  percent: string | number;
};

const ProductChartRadarComponent = ({
  dataRadaChart,
}: {
  dataRadaChart: RadarChartType[];
}) => {
  const [chartStyle, setChartStyle] = useState({
    style: { fontSize: "1.05rem" },
    outerRadius: 100,
    width: 560,
    height: 250,
  });

  const windowWidth = useWindowWidth();

  useEffect(() => {
    if (windowWidth <= 430) {
      setChartStyle({
        style: { fontSize: "0.45rem" },
        outerRadius: 50,
        width: 245,
        height: 150,
      });
      return;
    }

    if (windowWidth <= 500) {
      setChartStyle({
        style: { fontSize: "0.65rem" },
        outerRadius: 75,
        width: 350,
        height: 250,
      });
      return;
    }

    if (windowWidth <= 578) {
      setChartStyle({
        style: { fontSize: "0.85rem" },
        outerRadius: 80,
        width: 415,
        height: 300,
      });
      return;
    }

    if (windowWidth <= 767) {
      setChartStyle({
        style: { fontSize: "0.9rem" },
        outerRadius: 90,
        width: 455,
        height: 250,
      });
      return;
    }

    if (windowWidth <= 992) {
      setChartStyle({
        style: { fontSize: "0.6rem" },
        outerRadius: 60,
        width: 320,
        height: 250,
      });
      return;
    }

    if (windowWidth <= 1199) {
      setChartStyle({
        style: { fontSize: "0.9rem" },
        outerRadius: 80,
        width: 435,
        height: 250,
      });
      return;
    }

    if (windowWidth >= 1200) {
      setChartStyle({
        style: { fontSize: "1.05rem" },
        outerRadius: 100,
        width: 540,
        height: 250,
      });
      return;
    }
  }, [windowWidth]);

  return (
    <div>
      <RadarChart
        outerRadius={chartStyle.outerRadius}
        style={chartStyle.style}
        width={chartStyle.width}
        height={chartStyle.height}
        data={dataRadaChart}
      >
        <PolarGrid />
        <PolarAngleAxis dataKey="title" />
        <PolarRadiusAxis domain={[0, 150]} tick={false} axisLine={false} />
        <Radar dataKey="percent" fill="#0798d4" fillOpacity={0.6} />
        <Tooltip />
      </RadarChart>
    </div>
  );
};

export default ProductChartRadarComponent;
