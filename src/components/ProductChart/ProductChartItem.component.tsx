import React from "react";
import { Link } from "react-router-dom";
import { IProducts } from "@app/types";
import { imageUrl, imgLinktoServer } from "@app/utils";
import ProductChartInfoComponent from "./ProductChartInfo.component";
import ProductChartLineComponent from "./ProductChartLine.component";
import ProductChartRadarComponent from "./ProductChartRadar.component";
import moment from "moment";
import { useLanguage, useTrans } from "@app/hooks";

const ProductChartItemComponent = ({ botData }: { botData: IProducts }) => {
  const trans = useTrans();

  const dataRadaChart = [
    {
      title: `Max deposit: ${
        botData?.account_live?.stats?.max_deposit_load?.toFixed(2) || 0
      }%`,
      percent: botData?.account_live?.stats?.max_deposit_load?.toFixed(2) || 0,
    },
    {
      title: `Long trades: ${
        botData?.account_live?.stats?.buy_win_rate?.toFixed(2) || 0
      }%`,
      percent: botData?.account_live?.stats?.buy_win_rate?.toFixed(2) || 0,
    },
    {
      title: `Profit Trades: ${
        botData?.account_live?.stats?.win_rate?.toFixed(2) || 0
      }%`,
      percent: botData?.account_live?.stats?.win_rate?.toFixed(2) || 0,
    },
    {
      title: `Maximum drawdown: ${
        botData?.account_live?.stats?.max_drawdown?.toFixed(2) || 0
      }%`,
      percent: botData?.account_live?.stats?.max_drawdown?.toFixed(2) || 0,
    },
    {
      title: `Short trades: ${
        botData?.account_live?.stats?.sell_win_rate?.toFixed(2) || 0
      }%`,
      percent: botData?.account_live?.stats?.sell_win_rate?.toFixed(2) || 0,
    },
    {
      title: `Loss trades: ${
        botData?.account_live?.stats?.loss_rate?.toFixed(2) || 0
      }%`,
      percent: botData?.account_live?.stats?.loss_rate?.toFixed(2) || 0,
    },
  ];

  const productLanguage = useLanguage<IProducts>(botData);

  return (
    <div className="home__chart">
      <div className="row">
        <div className="home-chart__logo">
          <div className="home-chart__img">
            <img
              src={imageUrl(botData?.image)}
              alt={productLanguage?.title || ""}
            />
          </div>
          <div className="home-chart__title">
            <Link to={`/products/${botData.slug}/view`}>
              <h4>{productLanguage?.title || botData.title}</h4>
              <p>{productLanguage?.excerpt || botData.excerpt}</p>
            </Link>
          </div>
        </div>
      </div>

      <div className="home-chart__row">
        <div className="row">
          <div className="col-md-3 col-sm-12">
            <ProductChartLineComponent
              dataChartLine={botData.account_live.stats.profit_rates_by_day}
              market={botData.account_live.server_name}
              percent={botData.account_live.stats.profit_rate}
              proportion={botData.account_live.leverage}
            />
          </div>

          <div className="col-md-6 col-sm-12">
            <div className="home-chart__right-mobile">
              <div className="row">
                <div className="col-6">
                  <div className="home-chart__right-reliability">
                    <div className="home-chart__right-reliability-img">
                      <img
                        src="/assets/images/Group 220.svg"
                        alt="columns chart"
                      ></img>
                    </div>
                    <p className="home-chart__right-reliability-text">
                      {trans("home_content.reliability")}
                    </p>
                  </div>
                </div>

                <div className="col-6">
                  <div className="home-chart__right-time">
                    <div className="home-chart__right-time-weeks">
                      {moment().diff(
                        botData.account_live.stats.start_time_trade * 1000,
                        "week"
                      )}
                    </div>
                    <p className="home-chart__right-time-text">{`${trans(
                      "weeks"
                    )} (${trans("since")} ${moment(
                      botData.account_live.stats.start_time_trade * 1000
                    ).year()})`}</p>
                  </div>
                </div>
              </div>
            </div>

            <ProductChartRadarComponent dataRadaChart={dataRadaChart} />
          </div>

          <div className="col-md-3 col-sm-12">
            <ProductChartInfoComponent botData={botData.account_live.stats} />
          </div>
        </div>
      </div>
    </div>
  );
};

export default ProductChartItemComponent;
