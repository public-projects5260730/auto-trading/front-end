import React from "react";
import { useTrans } from "@app/hooks";
import moment from "moment";
import { IStats } from "@app/types";
import ProductChartInfoItemComponent from "./ProductChartInfoItem.component";

const ProductChartInfoComponent = ({ botData }: { botData: IStats }) => {
  const trans = useTrans();

  return (
    <div className="home-chart__right">
      <div className="home-chart__right-pc">
        <div className="row">
          <div className="col-6">
            <div className="home-chart__right-reliability">
              <div className="home-chart__right-reliability-img">
                <img
                  src="/assets/images/Group 220.svg"
                  alt="columns chart"
                ></img>
              </div>
              <p className="home-chart__right-reliability-text">
                {trans("home_content.reliability")}
              </p>
            </div>
          </div>

          <div className="col-6">
            <div className="home-chart__right-time">
              <div className="home-chart__right-time-weeks">
                {moment().diff(botData.start_time_trade * 1000, "week")}
              </div>
              <p className="home-chart__right-time-text">{`${trans(
                "weeks"
              )} (${trans("since")} ${moment(
                botData.start_time_trade * 1000
              ).year()})`}</p>
            </div>
          </div>
        </div>
      </div>

      <ProductChartInfoItemComponent text="equity" value={botData.equity} />
      <ProductChartInfoItemComponent text="profit" value={botData.profit_net} />
      <ProductChartInfoItemComponent
        text="initial_deposit"
        value={botData.deposit_init}
      />
      <ProductChartInfoItemComponent
        text="withdrawals"
        value={botData.withdrawals_total}
      />
      <ProductChartInfoItemComponent
        text="deposits"
        value={botData.deposit_total}
      />
    </div>
  );
};

export default ProductChartInfoComponent;
