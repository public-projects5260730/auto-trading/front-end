import React, { useEffect, useState } from "react";
import { IProfitByDay } from "@app/types";
import { Area, AreaChart, XAxis, Tooltip } from "recharts";
import { useWindowWidth } from "@app/hooks";

const ProductChartLineComponent = ({
  dataChartLine,
  market,
  proportion,
  percent,
}: {
  dataChartLine: IProfitByDay[];
  market: string;
  proportion: string;
  percent: number;
}) => {
  const [widthChart, setWidthChart] = useState(260);
  const [heightChart, setHeightChart] = useState(210);
  const windowWidth = useWindowWidth();

  useEffect(() => {
    if (windowWidth <= 430) {
      setWidthChart(270);
      setHeightChart(150);
      return;
    }

    if (windowWidth <= 500) {
      setWidthChart(370);
      setHeightChart(180);
      return;
    }

    if (windowWidth <= 578) {
      setWidthChart(450);
      setHeightChart(190);
      return;
    }

    if (windowWidth <= 767) {
      setWidthChart(480);
      setHeightChart(210);
      return;
    }

    if (windowWidth <= 992) {
      setWidthChart(160);
      setHeightChart(160);
      return;
    }

    if (windowWidth <= 1199) {
      setWidthChart(200);
      setHeightChart(180);
      return;
    }

    if (windowWidth >= 1200) {
      setWidthChart(260);
      setHeightChart(210);
      return;
    }
  }, [windowWidth]);

  return (
    <div className="home-chart__line">
      <p className="home-chart__line-percent">{percent}%</p>
      <AreaChart width={widthChart} height={heightChart} data={dataChartLine}>
        <XAxis dataKey="time" axisLine={false} tickLine={false} tick={false} />
        <defs>
          <linearGradient id="colorUv" x1="0" y1="0" x2="0" y2="1">
            <stop offset="20%" stopColor="#0798d4" stopOpacity={0.4} />
            <stop offset="95%" stopColor="#0798d4" stopOpacity={0} />
          </linearGradient>
        </defs>
        <Area
          type="monotone"
          dataKey="value"
          stroke="##0798d4"
          fillOpacity={1}
          fill="url(#colorUv)"
        />
        <Tooltip />
      </AreaChart>

      <div className="home-chart__line-text">
        <div className="home-chart__line-left">
          <img
            src="/assets/images/Group 294.svg"
            alt="shield check"
            className="home-chart__line-left-icon"
          />
          <p>{market}</p>
        </div>
        <h6>{proportion}</h6>
      </div>
    </div>
  );
};

export default ProductChartLineComponent;
