import React from "react";
import { useTrans } from "@app/hooks";
import { solid } from "@fortawesome/fontawesome-svg-core/import.macro";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { Link } from "react-router-dom";
import { faSkype, faTelegram } from "@fortawesome/free-brands-svg-icons";

const FooterComponent = () => {
  const trans = useTrans();

  return (
    <footer className="space_section">
      <div className="container">
        <div className="footer-grid__blook-top">
          <div className="footer__logo">
            <Link to="/" className="footer__logo-link">
              <img
                src="/assets/logo/logo_2.svg"
                alt="TimeTrading_logo"
                className="footer__logo-img"
              />
            </Link>
          </div>

          <div className="footer__payment">
            <h5 className="footer__payment-title">
              {trans("footer.accept_payments")}:
            </h5>
            <ul className="footer-payment__list">
              <li className="footer-payment__list-item">
                <img
                  src="/assets/images/Layer 166.svg"
                  alt="PayPal"
                  className="footer-payment__list-item-img"
                />
              </li>
              <li className="footer-payment__list-item">
                <img
                  src="/assets/images/Layer 167.svg"
                  alt="Visa MasterCard"
                  className="footer-payment__list-item-img"
                />
              </li>
              <li className="footer-payment__list-item">
                <img
                  src="/assets/images/Layer 169.svg"
                  alt="bitcoin"
                  className="footer-payment__list-item-img"
                />
              </li>
            </ul>
          </div>
        </div>

        <div className="footer-grid__blook-bottom">
          <div className="footer-info__contact">
            <h5 className="footer__info-title">{trans("contact_us")}</h5>
            <ul className="footer-info__contact-list">
              <li className="footer-info__contact-list-item">
                <Link to="" className="footer-info__contact-list-link">
                  <FontAwesomeIcon
                    icon={faSkype}
                    className="footer-info__contact-list-icon"
                  />
                  {trans("skype")}
                </Link>
              </li>
              <li className="footer-info__contact-list-item">
                <Link to="" className="footer-info__contact-list-link">
                  <FontAwesomeIcon
                    icon={faTelegram}
                    className="footer-info__contact-list-icon"
                  />
                  Telegram
                </Link>
              </li>
              <li className="footer-info__contact-list-item">
                <FontAwesomeIcon
                  icon={solid("envelope")}
                  className="footer-info__contact-list-icon"
                />
                {trans("footer.mail")}
              </li>
            </ul>
          </div>

          <div className="footer-info__work">
            <h5 className="footer__info-title">
              {trans("footer.office_hours")}
            </h5>
            <p className="footer-info__work-paragraph">
              {trans("footer.work_days")}
            </p>
            <p className="footer-info__work-paragraph">
              {trans("footer.work_hours")}
            </p>
            <p className="footer-info__work-paragraph">
              {trans("footer.sundays")}
            </p>
            <p className="footer-info__work-paragraph">
              {trans("footer.join_us")}
            </p>
          </div>

          <div className="footer-info__information">
            <h5 className="footer__info-title">{trans("information")}</h5>
            <ul className="footer-info__information-list">
              <li className="footer-info__information-list-item">
                <Link to={""} className="footer-info__information-list-link">
                  {trans("about")}
                </Link>
              </li>
              <li className="footer-info__information-list-item">
                <Link to={""} className="footer-info__information-list-link">
                  {trans("user_guide")}
                </Link>
              </li>
              <li className="footer-info__information-list-item">
                <Link to={""} className="footer-info__information-list-link">
                  {trans("customer_care")}
                </Link>
              </li>
            </ul>
          </div>

          <div className="footer-info__last">
            <div className="footer-info__socials">
              <h5 className="footer__info-title">
                {trans("footer.follow_us")}
              </h5>
              <ul className="footer-info__socials-list">
                <li className="footer-info__socials-list-item">
                  <Link to={""} className="footer-info__socials-list-link">
                    <img
                      src="/assets/images/facebook.svg"
                      alt="facebook"
                      className="footer-info__socials-list-icon"
                    />
                  </Link>
                </li>
                <li className="footer-info__socials-list-item">
                  <Link to={""} className="footer-info__socials-list-link">
                    <img
                      src="/assets/images/instagram.svg"
                      alt="instagram"
                      className="footer-info__socials-list-icon"
                    />
                  </Link>
                </li>
                <li className="footer-info__socials-list-item">
                  <Link to={""} className="footer-info__socials-list-link">
                    <img
                      src="/assets/images/twitter.svg"
                      alt="twitter"
                      className="footer-info__socials-list-icon"
                    />
                  </Link>
                </li>
              </ul>
            </div>

            <div className="footer-info__devices">
              <ul className="footer-info__devices-list">
                <li className="footer-info__devices-list-item">
                  <div>
                    <img
                      src="/assets/images/window.svg"
                      alt="windows"
                      className="footer-info__devices-list-item-img"
                    />
                  </div>
                  <p className="footer-info__devices-list-item-p">
                    {trans("Windows")}
                  </p>
                </li>
                <li className="footer-info__devices-list-item">
                  <div>
                    <img
                      src="/assets/images/apple.svg"
                      alt="apple"
                      className="footer-info__devices-list-item-img"
                    />
                  </div>
                  <p className="footer-info__devices-list-item-p">
                    {trans("Iphone_Ipad")}
                  </p>
                </li>
                <li className="footer-info__devices-list-item">
                  <div>
                    <img
                      src="/assets/images/android.svg"
                      alt="android"
                      className="footer-info__devices-list-item-img"
                    />
                  </div>
                  <p className="footer-info__devices-list-item-p">
                    {trans("android")}
                  </p>
                </li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </footer>
  );
};

export default FooterComponent;
