export interface ILanguageState {
  code?: string;
  title?: string;
}
