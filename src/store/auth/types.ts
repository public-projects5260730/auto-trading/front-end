import { IUser } from "@app/types";

export interface IAuthState {
  visitor: IUser | null;
  accessToken: string;
  status: "idle" | "loading" | "failed";
}
