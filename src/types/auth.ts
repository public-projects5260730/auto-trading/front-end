export interface IUser {
  id: number;
  name: string;
  email: string;
  avatar?: string;
  permissions?: Record<string, boolean>;
}

export interface ILoginData {
  email: string;
  password: string;
  rememberMe: boolean;
}

export interface IRegisterData {
  name: string;
  email: string;
  password: string;
  password_confirmation: string;
}

export interface IChangeEmailData {
  email: string;
  password: string;
}

export interface IChangePasswordData {
  old_password: string;
  password: string;
  password_confirmation: string;
}

export interface IForgotPasswordFormData {
  email: string;
}

export interface IResetPasswordFormData {
  token: string;
  email: string;
  password: string;
  password_confirmation: string;
}

