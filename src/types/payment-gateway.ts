export interface IPaymentGateway {
  id: number;
  provider_id: string;
  title: string;
  image?: string;
  order: number;
  active: number;
}
