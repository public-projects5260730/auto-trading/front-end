export interface IProducts {
  id: number;
  title: string;
  excerpt: string;
  overview: string;
  guide: string;
  image: string;
  files: string;
  slug: string;
  status: number;
  featured: number;
  order: number;
  created_at: number;
  updated_at: number;
  category_id: number;
  account_live_id: number;
  backtest_images: string | null;
  comments: string;
  versions: string;
  trans: ITrans[] | null;
  account_live: IAccountLive;
  language?: string;
}

export interface ITrans {
  language: string;
  id: number;
  title: string;
  excerpt: string;
  overview: string;
  guide: string;
  image: string;
  files: string;
  slug: string;
  status: number;
  featured: number;
  order: number;
  created_at: number;
  updated_at: number;
  category_id: number;
  account_live_id: number;
  backtest_images: string | null;
  comments: string;
  versions: string;
  trans: null;
}

export interface IAccountLive {
  id: number;
  account_name: string;
  created_at: Date;
  currency: string;
  leverage: string;
  server_name: string;
  updated_at: Date;
  bot_code: null;
  stats: IStats;
}

export interface IStats {
  id: number;
  avg_loss_trade: number;
  avg_profit_trade: number;
  avg_time_trade: number;
  balance: number;
  best_pips: number;
  best_profit: number;
  buy_trades: number;
  buy_win_rate: number;
  buy_win_trades: number;
  commission_total: number;
  created_at: string;
  credit: number;
  deposit_init: number;
  deposit_total: number;
  equity: number;
  loss_rate: number;
  loss_total: number;
  loss_trades: number;
  lot_max: number;
  lot_min: number;
  lot_total: number;
  max_deposit_load: number;
  max_drawdown: number;
  max_loss_trade: number;
  max_win_trade: number;
  pips_total: number;
  profit_factor: number;
  profit_net: number;
  profit_rate: number;
  profit_running: number;
  profit_total: number;
  sell_trades: number;
  sell_win_rate: number;
  sell_win_trades: number;
  start_time_trade: number;
  swap_total: number;
  time_best_pips: number;
  time_best_profit: number;
  time_trade_total: number;
  time_worst_pips: number;
  time_worst_profit: number;
  total_trades: number;
  updated_at: string;
  win_rate: number;
  win_trades: number;
  withdrawals_total: number;
  worst_pips: number;
  worst_profit: number;
  account_live_id: number;
  profit_rates_by_day: IProfitByDay[];
  profit_by_day: IProfitByDay[];
  gain_collection: IProfitByDay[];
  symbol_collection: ISymbolCollection[];
}

export interface IProfitByDay {
  time: number;
  value: number;
}

export interface ISymbolCollection {
  symbol: string;
  countSymbol: number;
}

export interface IProductsPaginatorParams
  extends Omit<IProductParams, "limit"> {
  perPage?: number;
  page?: number;
}

export interface IProductParams {
  limit?: number;
  featured?: boolean | 0 | 1;
  category_id?: number;
  orders?: Record<string, string>;
  __not?: Record<string, unknown>;
  title?: string;
  search?: string;
}

export interface IRealtime {
  id: number;
  close_price: number;
  close_time: number;
  comment: string;
  commission: number;
  created_at: number;
  duration: number;
  lot: number;
  open_price: number;
  open_time: number;
  order_type: number;
  pips: number;
  profit_gross: number;
  profit_net: number;
  sl_price: number;
  swap: number;
  symbol: string;
  ticket: number;
  tp_price: number;
  updated_at: number;
  account_live_id: number;
}

export interface IRealtimeFilter {
  page: number;
  toDate?: number;
  fromDate?: number;
}
