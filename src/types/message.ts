export interface IMessage {
  name: string;
  email: string;
  title: string;
  message: string;
}
