export { };
declare global {
  interface Window {
    _env: {
      REACT_APP_SITE_NAME: string;
      REACT_APP_API_URL: string;
      REACT_APP_ACCOUNT_LIVE_REFRESH_TIMER: number;
    };
  }
}
