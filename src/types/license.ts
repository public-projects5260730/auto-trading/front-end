export interface ILicense {
  id: number;
  title: string;
  account_number: number;
  license_days: number;
  cost_amount: string;
  cost_currency: string;
  is_trial: number;
  order: number;
  active: number;
  created_at: Date;
  updated_at: Date;
  trans: ILicense[] | null;
}

export interface IPurchaseLicenseFormData {
  email?: string;
  license_id?: number;
  payment_profile_id?: number;
}
