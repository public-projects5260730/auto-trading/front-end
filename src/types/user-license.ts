import { ILicense, IProducts } from "@app/types";

export interface IUserLicense {
  id: number;
  license_key: string;
  user_id: number;
  product_id: number;
  license_id: number;
  purchase_request_key: string;
  account_number: number;
  license_days: number;
  activated_accounts: number[];
  state: number;
  reason: string;
  activated_at: number;
  expired_at: number;
  created_at: number;
  updated_at: number;
  is_trial: boolean;
  is_synced: boolean;
  email: string;
  product?: IProducts;
  license?: ILicense;
  permissions?: Record<string, boolean>
}