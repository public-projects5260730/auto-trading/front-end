export interface IFAQGroup {
  id: number;
  title: string;
  trans: IFAQGroup[] | null;
  faqs?: IFAQ[]
}

export interface IFAQ {
  id: number;
  title: string;
  content: string;
  order: number;
  created_at: Date;
  updated_at: Date;
  trans: IFAQ[] | null;
}
