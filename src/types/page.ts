export interface IPage {
  id: number;
  title: string;
  slug: string;
  body: string;
  active: boolean;
  trans: IPage[] | null;
}
