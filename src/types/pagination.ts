export interface IPagination {
  page: number;
  perPage: number;
  total: number;
  resultCount: number;
}

export interface IPaginationData<T> extends IPagination {
  data: T[];
}

export interface IPaginationFilters {
  page?: number;
  perPage?: number;
}
