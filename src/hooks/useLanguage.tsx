import { selectLanguage } from "@app/store/language";
import { useEffect, useState } from "react";
import { useAppSelector } from "./useReduxHooks";

export const useLanguage = <T,>(data: any): T => {
  const language = useAppSelector(selectLanguage);
  const [languageData, setLanguageData] = useState<T>(data);

  useEffect(() => {
    if (Array.isArray(data)) {
      setLanguageData(
        data.map(
          (_data: any) =>
            _data?.trans?.find(
              (tran: any) => tran?.language === (language?.code || "en")
            ) || _data
        ) as T
      );
      return;
    }
    setLanguageData(
      data?.trans?.find(
        (tran: any) => tran?.language === (language?.code || "en")
      ) || data
    );
  }, [language?.code, data]);

  return languageData;
};
