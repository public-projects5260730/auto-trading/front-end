import { useLocation, useNavigate } from "react-router-dom";

export const useReloadPage = () => {
  const history = useNavigate();
  const { pathname } = useLocation();

  const reloadPage = (path?: string) => {
    if (path?.length && path !== pathname) {
      history(path);
      return;
    }
    history("/empty");
    setTimeout(() => history(pathname, { replace: true }), 0);
  };

  return reloadPage;
};
