import { IApiResponse, IPage } from "@app/types";
import axiosClient from "./axiosClient";

export const pageApi = {
  getPageView: (slug: string): Promise<IApiResponse<{ page?: IPage }>> => {
    const url = `/pages/${slug}/view`;
    return axiosClient.get(url);
  },
};
