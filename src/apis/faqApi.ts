import { IApiResponse, IFAQGroup } from "@app/types";
import axiosClient from "./axiosClient";

export const faqApi = {
  fetchGroups: (): Promise<IApiResponse<{ faqGroups?: IFAQGroup[] }>> => {
    const url = `faqs/groups`;
    return axiosClient.get(url);
  },
};
