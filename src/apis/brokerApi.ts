import { IApiResponse } from "@app/types";
import axiosClient from "./axiosClient";

export const brokerApi = {
  fetchAll: (limit = 20): Promise<IApiResponse<{ brokers?: any }>> => {
    const url = `/brokers`;
    return axiosClient.get(url, { params: { limit } });
  },
};
