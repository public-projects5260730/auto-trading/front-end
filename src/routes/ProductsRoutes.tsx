import React from "react";
import { ProductsPage, ProductViewPage } from "@app/features";
import { Route, Routes } from "react-router-dom";

const ProductsRoutes = () => {
  return (
    <Routes>
      <Route index element={<ProductsPage />} />
      <Route path=":tabs" element={<ProductsPage />} />
      <Route path=":slug/view" element={<ProductViewPage />} />
    </Routes>
  );
};

export default ProductsRoutes;
