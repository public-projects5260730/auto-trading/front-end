import { UserPage } from "@app/features/user";
import { Route, Routes } from "react-router-dom";

const UserRoutes = () => {
  return (
    <Routes>
      <Route index element={<UserPage />} />
      <Route path="/:tabs" element={<UserPage />} />
    </Routes>
  );
};

export default UserRoutes;
