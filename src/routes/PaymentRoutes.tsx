import { PaymentPage, PaymentSuccessPage } from "@app/features";
import { Route, Routes } from "react-router-dom";

const PaymentRoutes = () => {
  return (
    <Routes>
      <Route index element={<PaymentPage />} />
      <Route path="success" element={<PaymentSuccessPage />} />
    </Routes>
  );
};

export default PaymentRoutes;
