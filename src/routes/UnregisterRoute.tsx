import { Navigate, Outlet } from "react-router-dom";
import { useAppSelector } from "@app/hooks";
import { selectVisitorId } from "@app/store/auth";

const UnregisterRoute = () => {
  const visitorId = useAppSelector(selectVisitorId);
  return !visitorId ? <Outlet /> : <Navigate to="/" />;
};
export default UnregisterRoute;
