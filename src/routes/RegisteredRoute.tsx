import { useAppSelector } from "@app/hooks";
import { selectVisitorId } from "@app/store/auth";
import { Outlet } from "react-router-dom";

import { LoginPage } from "@app/features/auth/login";

const RegisteredRoute = () => {
  const visitorId = useAppSelector(selectVisitorId);
  return visitorId ? <Outlet /> : <LoginPage />;
};
export default RegisteredRoute;
