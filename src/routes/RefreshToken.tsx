import { useAppDispatch, useAppSelector } from "@app/hooks";
import { authActions, selectAccessToken } from "@app/store/auth";

import { useEffect } from "react";
import jwt_decode from "jwt-decode";

const RefreshToken = () => {
  const accessToken = useAppSelector(selectAccessToken);
  const dispatch = useAppDispatch();

  useEffect(() => {
    if (!accessToken?.length) {
      return;
    }

    const tokenDecode = jwt_decode<{ exp?: number }>(accessToken);
    const now = Date.now();
    const timeWaiter = Math.max(0, ((tokenDecode?.exp || 0) - 60) * 1000 - now);
    const timeOut = setTimeout(
      () => dispatch(authActions.refreshToken()),
      timeWaiter
    );

    return () => {
      clearTimeout(timeOut);
    };
  }, [accessToken, dispatch]);

  return <></>;
};
export default RefreshToken;
