import { ContactUsPage } from "@app/features";
import React from "react";
import { Route, Routes } from "react-router-dom";

const ContactUsRoutes = () => {
  return (
    <Routes>
      <Route index element={<ContactUsPage />} />
    </Routes>
  );
};

export default ContactUsRoutes;
