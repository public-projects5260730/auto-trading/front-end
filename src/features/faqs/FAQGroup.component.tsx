import { useLanguage } from "@app/hooks";
import { IFAQGroup } from "@app/types";
import { ProductViewFQAItemComponent } from "../products/component";

const FAQGroupComponent = ({ group }: { group: IFAQGroup }) => {
  const groupLanguage = useLanguage<IFAQGroup>(group);

  return (
    <div className="products-view">
      <div className="products-view__content">
        <div className="products-view__FQA border-0">
          <h4 className="mb-3">{groupLanguage.title}</h4>
          {group?.faqs?.map((faq) => (
            <ProductViewFQAItemComponent key={faq.id} item={faq} />
          ))}
        </div>
      </div>
    </div>
  );
};

export default FAQGroupComponent;
