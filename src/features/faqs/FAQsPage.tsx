import { faqApi } from "@app/apis";
import {
  LoadingComponent,
  MainTopComponent,
  NoDataComponent,
} from "@app/components";
import { useHeader, useTrans } from "@app/hooks";
import { IFAQGroup } from "@app/types";
import { useEffect, useState } from "react";
import { from } from "rxjs";
import FAQGroupComponent from "./FAQGroup.component";

const FAQsPage = () => {
  const trans = useTrans();
  const [faqGroups, setFaqGroups] = useState<IFAQGroup[]>();

  useHeader({
    pageTitle: trans("faqs").toString(),
    breadcrumbs: [
      {
        title: trans("faqs"),
        active: true,
      },
    ],
  });

  useEffect(() => {
    const subscription = from(faqApi.fetchGroups()).subscribe(
      ({ faqGroups }) => {
        setFaqGroups(faqGroups || []);
      }
    );

    return () => {
      subscription.unsubscribe();
    };
  }, []);

  return (
    <div>
      <MainTopComponent />
      <div id="faq-page" className="container mt-5 mb-5">
        {faqGroups === undefined && <LoadingComponent />}
        {!faqGroups?.length && <NoDataComponent />}
        {faqGroups
          ?.filter(({ faqs }) => faqs?.length)
          ?.map((group) => (
            <FAQGroupComponent key={group.id} group={group} />
          ))}
      </div>
    </div>
  );
};

export default FAQsPage;
