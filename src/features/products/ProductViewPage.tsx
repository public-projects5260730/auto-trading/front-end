import React from "react";
import { useTrans } from "@app/hooks";
import { useParams } from "react-router-dom";
import { ProductViewContainer } from "./component";
import {
  ProductRealtimeContext,
  ProductViewPageContext,
  useRealtimeData,
  useViewData,
} from "./feature";

const ProductViewPage = () => {
  const trans = useTrans();
  const { slug } = useParams<{ slug: string }>();

  if (!slug) {
    return <div>{trans("miss-product")}</div>;
  }

  return <ProductViewProviderComponent slug={slug} />;
};

const ProductViewProviderComponent = ({ slug }: { slug: string }) => {
  const pageData = useViewData(slug);
  const realtimeData = useRealtimeData(slug);

  return (
    <ProductViewPageContext.Provider value={pageData}>
      <ProductRealtimeContext.Provider value={realtimeData}>
        <ProductViewContainer slug={slug} />
      </ProductRealtimeContext.Provider>
    </ProductViewPageContext.Provider>
  );
};

export default ProductViewPage;
