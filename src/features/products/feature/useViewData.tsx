import { useEffect, useState, useMemo } from "react";
import { productApi } from "@app/apis";
import { IPaginationData, IProducts, IRealtime } from "@app/types";
import { IFAQ } from "@app/types/FQA";
import { useLocation, useNavigate } from "react-router-dom";
import { concatMap, from, timer } from "rxjs";
import { useFilters } from "@app/hooks";
import { intervalDurationTimer } from "@app/utils";

export function useViewData(slug: string) {
  const [tabActive, setTabActive] = useState("overview");

  // product data
  const [productData, setProductData] = useState<IProducts>();
  const [productLoadingStatus, setProductLoadingStatus] = useState<
    "loading" | "done" | "error"
  >("loading");
  const [productError, setProductError] = useState<string>();

  // products slide data
  const [slidesData, setSlidesData] = useState<IProducts[]>();
  const [slidesloadingStatus, setSlidesLoadingStatus] = useState<
    "loading" | "done" | "error"
  >("loading");
  const [slidesError, setSlidesError] = useState<string>();

  // product FQA data
  const [fAQData, setFAQData] = useState<IFAQ[]>();
  const [fAQLoadingStatus, setFAQLoadingStatus] = useState<
    "loading" | "done" | "error"
  >("loading");
  const [fAQError, setFAQError] = useState<string>();

  // get product data
  useEffect(() => {
    const subscription = timer(0, intervalDurationTimer())
      .pipe(concatMap(() => productApi.getProductView(slug)))
      .subscribe(({ product, error }) => {
        if (product) {
          setProductData(product);
          setProductLoadingStatus("done");
        }

        if (error) {
          setProductLoadingStatus("error");
          setProductError(error);
        }
      });

    return () => {
      subscription.unsubscribe();
    };
  }, [slug]);

  // get products slide data
  useEffect(() => {
    const subscription = from(productApi.getProductsList(9)).subscribe(
      ({ products, error }) => {
        if (products) {
          setSlidesData(products);
          setSlidesLoadingStatus("done");
        }

        if (error) {
          setSlidesLoadingStatus("error");
          setSlidesError(error);
        }
      }
    );

    return () => {
      subscription.unsubscribe();
    };
  }, []);

  // get product FQA data
  useEffect(() => {
    const subscription = from(productApi.getProductFAQ(slug)).subscribe(
      ({ faqs, error }) => {
        if (faqs) {
          setFAQData(faqs);
          setFAQLoadingStatus("done");
        }

        if (error) {
          setFAQLoadingStatus("error");
          setFAQError(error);
        }
      }
    );

    return () => {
      subscription.unsubscribe();
    };
  }, [slug]);

  return {
    productData,
    productLoadingStatus,
    productError,
    slidesData,
    slidesloadingStatus,
    slidesError,
    fAQData,
    fAQLoadingStatus,
    fAQError,
    tabActive,
    setTabActive,
  };
}

export function useRealtimeData(slug: string) {
  const location = useLocation();
  const navigate = useNavigate();

  const { filters } = useFilters();
  const { page } = filters;

  const [tabRealtimeActive, setTabRealtimeActive] = useState("trading_history");
  const [rangeDate, setRangerDate] = useState("");

  const [realtimeData, setRealtimeData] = useState<
    IPaginationData<IRealtime>
  >();
  const [realtimeLoadingStatus, setRealtimeLoadingStatus] = useState<
    "loading" | "done" | "error"
  >("loading");
  const [realtimeError, setRealtimeError] = useState<string>();

  const [fromDate, setFromDate] = useState<Date | null>(null);
  const [fromDateTimestamp, setFromDateTimeStamp] = useState<number>(0);

  const [toDate, setToDate] = useState<Date | null>(null);
  const [toDateTimestamp, setToDateTimeStamp] = useState<number>(0);

  const handleRangeDate = (rangeDateType: string) => {
    setRangerDate(rangeDateType);
    switch (rangeDateType) {
      case "today": {
        const date = new Date();
        setFromDate(date);
        setToDate(null);
        break;
      }

      case "this_month": {
        const date = new Date();
        date.setDate(1);
        setFromDate(date);
        setToDate(null);
        break;
      }

      case "last_month": {
        const fromDate = new Date();
        fromDate.setDate(1);
        fromDate.setMonth(fromDate.getMonth() - 1);
        setFromDate(fromDate);

        const toDate = new Date();
        toDate.setDate(0);
        setToDate(toDate);
        break;
      }
      default:
        break;
    }
  };

  useMemo(() => {
    if (page !== 1) {
      navigate(location.pathname);
    }

    if (fromDate) {
      setFromDateTimeStamp(
        Math.round(new Date(fromDate).setUTCHours(0, 0, 0, 0) / 1000)
      );
    }

    if (toDate) {
      setToDateTimeStamp(
        Math.round(new Date(toDate).setUTCHours(23, 59, 59, 0) / 1000)
      );
    }

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [fromDate, toDate]);

  useEffect(() => {
    const subscription = from(
      productApi.getProductRealtime(slug, {
        page: page || 1,
        fromDate: fromDateTimestamp,
        toDate: toDateTimestamp,
      })
    ).subscribe(({ paginate, error }) => {
      if (paginate) {
        setRealtimeData(paginate);
        setRealtimeLoadingStatus("done");
      }

      if (error) {
        setRealtimeLoadingStatus("error");
        setRealtimeError(error);
      }
    });

    return () => {
      subscription.unsubscribe();
    };
  }, [slug, page, fromDateTimestamp, toDateTimestamp]);

  return {
    realtimeData,
    realtimeLoadingStatus,
    realtimeError,
    fromDate,
    setFromDate,
    toDate,
    setToDate,
    tabRealtimeActive,
    setTabRealtimeActive,
    rangeDate,
    handleRangeDate,
  };
}
