import { useState, useEffect, useMemo } from "react";
import { productApi } from "@app/apis";
import { IPaginationData, IProducts } from "@app/types";
import { from } from "rxjs";
import { useFilters } from "@app/hooks";
import { useLocation, useNavigate } from "react-router-dom";

export function useProductPageData() {
  const location = useLocation();
  const navigate = useNavigate();

  const [tabActive, setTabActive] = useState("growth");

  const { filters } = useFilters();

  const [paginatorData, setPaginatorData] =
    useState<IPaginationData<IProducts>>();
  const [loadingStatus, setLoadingStatus] = useState<
    "loading" | "done" | "error"
  >("loading");
  const [error, setError] = useState<string>();

  const [order, setOrder] = useState<Record<string, string>>({
    profit_rate: "DESC",
  });

  const { page, search } = filters;

  useMemo(() => {
    switch (tabActive) {
      case "maximum_drawdown":
        setOrder({ max_drawdown: "ASC" });
        break;
      case "profit":
        setOrder({ profit_rate: "DESC" });
        break;
      case "top_download":
        setOrder({ top_download: "DESC" });
        break;
      case "winrate":
        setOrder({ win_rate: "DESC" });
        break;
      default:
        setOrder({ profit_rate: "DESC" });
        break;
    }
  }, [tabActive]);

  useEffect(() => {
    if (page !== 1) {
      navigate(location.pathname);
    }

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [search]);

  useEffect(() => {
    const subscription = from(
      productApi.getProductsPaginate({
        perPage: 9,
        orders: order,
        page: page || 1,
        search: search || "",
      })
    ).subscribe(({ paginate, error }) => {
      if (paginate) {
        setPaginatorData(paginate);
        setLoadingStatus("done");
      }

      if (error) {
        setLoadingStatus("error");
        setError(error);
      }
    });

    return () => {
      subscription.unsubscribe();
    };
  }, [order, page, search]);

  return {
    error,
    loadingStatus,
    paginatorData,
    tabActive,
    setTabActive,
  };
}
