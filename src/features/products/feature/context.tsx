import { createContext, Dispatch, SetStateAction } from "react";
import { IPaginationData, IProducts, IRealtime } from "@app/types";
import { IFAQ } from "@app/types/FQA";

type ProductPageTypeContext = {
  loadingStatus: "loading" | "done" | "error";
  paginatorData?: IPaginationData<IProducts>;
  error?: string;
  tabActive: string;
  setTabActive: Dispatch<SetStateAction<string>>;
};

type ProductViewPageTypeContext = {
  productData?: IProducts;
  productLoadingStatus: "loading" | "done" | "error";
  productError?: string;
  slidesData?: IProducts[];
  slidesloadingStatus: "loading" | "done" | "error";
  slidesError?: string;
  fAQData?: IFAQ[];
  fAQLoadingStatus: "loading" | "done" | "error";
  fAQError?: string;
  tabActive: string;
  setTabActive: Dispatch<SetStateAction<string>>;
};

type ProductRealtimeTypeContext = {
  realtimeData?: IPaginationData<IRealtime>;
  realtimeLoadingStatus: "loading" | "done" | "error";
  realtimeError?: string;
  toDate: Date | null;
  setToDate: Dispatch<SetStateAction<Date | null>>;
  fromDate: Date | null;
  setFromDate: Dispatch<SetStateAction<Date | null>>;
  tabRealtimeActive: string;
  setTabRealtimeActive: Dispatch<SetStateAction<string>>;
  rangeDate: string;
  handleRangeDate: (rangeDateType: string) => void;
};

export const ProductPageContext = createContext<ProductPageTypeContext>({
  loadingStatus: "loading",
  tabActive: "",
  setTabActive: () => "",
});

export const ProductViewPageContext = createContext<ProductViewPageTypeContext>(
  {
    productLoadingStatus: "loading",
    slidesloadingStatus: "loading",
    fAQLoadingStatus: "loading",
    tabActive: "",
    setTabActive: () => "",
  }
);

export const ProductRealtimeContext = createContext<ProductRealtimeTypeContext>(
  {
    realtimeLoadingStatus: "loading",
    toDate: null,
    setToDate: () => {},
    fromDate: null,
    setFromDate: () => {},
    tabRealtimeActive: "",
    setTabRealtimeActive: () => "",
    rangeDate: "",
    handleRangeDate: (rangeDateType: string) => {
      //
    },
  }
);
