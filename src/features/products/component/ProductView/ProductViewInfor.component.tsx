import React, { useContext } from "react";
import { IProducts } from "@app/types";
import { ProductViewPageContext } from "../../feature";
import {
  ProductViewTabItemComponent,
  ProductViewOverViewComponent,
  ProductViewRealtimeComponent,
  ProductViewBacktestComponent,
  ProductViewUserCommentComponent,
  ProductViewWhatNewComponent,
  ProductViewFQAComponent,
} from "./ProductViewInfo";
import { useLanguage } from "@app/hooks";

const ProductViewInforComponent = ({
  productData,
}: {
  productData: IProducts;
}) => {
  const { tabActive, setTabActive } = useContext(ProductViewPageContext);
  const productLanguage = useLanguage<IProducts>(productData);

  return (
    <div className="products-view__info">
      <div className="products-view__wrapper-top">
        <ul className="products-view__info-list">
          <ProductViewTabItemComponent
            setTabActive={setTabActive}
            tabActive={tabActive}
            tabName="overview"
          />
          <ProductViewTabItemComponent
            setTabActive={setTabActive}
            tabActive={tabActive}
            tabName="realtime"
          />
          <ProductViewTabItemComponent
            setTabActive={setTabActive}
            tabActive={tabActive}
            tabName="backtest"
          />
          <ProductViewTabItemComponent
            setTabActive={setTabActive}
            tabActive={tabActive}
            tabName="user_comment"
          />
          <ProductViewTabItemComponent
            setTabActive={setTabActive}
            tabActive={tabActive}
            tabName="what_new"
          />
          {/* <ProductViewTabItemComponent
            setTabActive={setTabActive}
            tabActive={tabActive}
            tabName="manuel_guide"
          /> */}
          <ProductViewTabItemComponent
            setTabActive={setTabActive}
            tabActive={tabActive}
            tabName="FQA"
          />
        </ul>
      </div>

      {tabActive === "overview" && (
        <div className="products-view__wrapper-bottom">
          <ProductViewOverViewComponent content={productLanguage.overview} />
          {/* <div className="products-view__overview-buy">
            <ProductViewInfoBuyButtonComponent
              link=""
              imgLink="/assets/images/free-trial.svg"
              imgAlt="free trial"
              text="Free trial"
              value="7 days free trial"
              classCustom="free-trial"
            />
            <ProductViewInfoBuyButtonComponent
              link=""
              imgLink="/assets/images/buy.svg"
              imgAlt="buy"
              text="buy"
              value="79$ - Full license"
              classCustom="buy"
            />
            <ProductViewInfoBuyButtonComponent
              link=""
              imgLink="/assets/images/rent.svg"
              imgAlt="rent"
              text="rent"
              value="1 month - 19$"
              classCustom="rent"
            />
            <ProductViewInfoBuyButtonComponent
              link=""
              imgLink="/assets/images/free.svg"
              imgAlt="free"
              text="Get it FREE"
              value="Open referred account"
              classCustom="free"
            />
          </div> */}
        </div>
      )}

      {tabActive === "realtime" && (
        <ProductViewRealtimeComponent productData={productData} />
      )}

      {tabActive === "backtest" && (
        <ProductViewBacktestComponent
          backtestImages={productData.backtest_images}
        />
      )}

      {tabActive === "user_comment" && (
        <ProductViewUserCommentComponent comment={productLanguage.comments} />
      )}

      {tabActive === "what_new" && (
        <ProductViewWhatNewComponent version={productLanguage.versions} />
      )}

      {/* {tabActive === "manuel_guide" && (
        <ProductViewManualGuideComponent guide={productLanguage.guide} />
      )} */}

      {tabActive === "FQA" && <ProductViewFQAComponent />}
    </div>
  );
};

export default ProductViewInforComponent;
