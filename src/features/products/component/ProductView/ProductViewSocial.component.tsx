import React from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faThumbsUp } from "@fortawesome/free-solid-svg-icons";
import { faTwitter, faFacebook } from "@fortawesome/free-brands-svg-icons";
import { useTrans } from "@app/hooks";

const ProductViewSocialComponent = ({
  likeNumber,
  shareNumber,
  twitterNumber,
}: {
  likeNumber: number;
  shareNumber: number;
  twitterNumber: number;
}) => {
  const trans = useTrans();

  return (
    <div className="products-view__social">
      <div className="products-view__social-item">
        <FontAwesomeIcon
          icon={faThumbsUp}
          className="products-view__social-icon like"
        />
        <p>
          {trans("like")} {likeNumber}
        </p>
      </div>
      <div className="products-view__social-item">
        <FontAwesomeIcon
          icon={faFacebook}
          className="products-view__social-icon facebook"
        />
        <p>
          {trans("share")} {shareNumber}
        </p>
      </div>
      <div className="products-view__social-item">
        <FontAwesomeIcon
          icon={faTwitter}
          className="products-view__social-icon twitter"
        />
        <p>Twitter {twitterNumber}</p>
      </div>
    </div>
  );
};

export default ProductViewSocialComponent;
