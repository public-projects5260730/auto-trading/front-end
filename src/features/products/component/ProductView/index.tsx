export { default as ProductViewSocialComponent } from "./ProductViewSocial.component";
export { default as ProductViewTopComponent } from "./ProductViewTop.component";
export { default as ProductViewSlideComponent } from "./ProductViewSlide.component";
export { default as ProductViewContainer } from "./ProductView.container";
export { default as ProductViewInforComponent } from "./ProductViewInfor.component";
export * from "./ProductViewInfo";
