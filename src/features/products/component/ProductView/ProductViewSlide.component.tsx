import React, { useContext } from "react";
import { Link } from "react-router-dom";
import { Autoplay, Navigation } from "swiper";
import { Swiper, SwiperSlide } from "swiper/react";
import { ProductViewPageContext } from "../../feature";
import { ErrorComponent, LoadingComponent } from "@app/components";
import { imgLinktoServer } from "@app/utils";
import { useTrans } from "@app/hooks";

const ProductViewSlideComponent = ({ slug }: { slug: string }) => {
  const trans = useTrans();

  const { slidesloadingStatus, slidesError, slidesData } = useContext(
    ProductViewPageContext
  );

  if (slidesloadingStatus === "loading") {
    return <LoadingComponent />;
  }

  if (slidesloadingStatus === "error") {
    return <ErrorComponent error={slidesError} />;
  }

  if (!slidesData || slidesData.length === 0) {
    return <></>;
  }

  const thisProduct = slidesData.find((item) => item.slug === slug);

  if (thisProduct && slidesData.length === 1) {
    return <></>;
  }

  return (
    <div className="products-view__slide">
      <h3>{trans("product.our_other_products")}</h3>
      <Swiper
        grabCursor={true}
        navigation={true}
        modules={[Navigation, Autoplay]}
        className="products-view__slide-swiper"
        loop={true}
        slidesPerView={3}
        breakpoints={{
          768: {
            slidesPerView: 5,
          },

          578: {
            slidesPerView: 4,
          },
        }}
      >
        {slidesData
          ?.filter((item) => item.slug !== slug)
          .map((item, index) => (
            <SwiperSlide
              key={index}
              className="products-view__slide-swiper-item"
            >
              <Link
                to={`/products/${item.slug}/view`}
                className="products-view__slide-swiper-link"
              >
                <img src={`${imgLinktoServer}${item.image}`} alt={item.title} />
              </Link>
            </SwiperSlide>
          ))}
      </Swiper>
    </div>
  );
};

export default ProductViewSlideComponent;
