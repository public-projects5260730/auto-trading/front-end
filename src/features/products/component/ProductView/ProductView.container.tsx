import React, { useContext } from "react";
import { useNavigate } from "react-router-dom";
import { ProductViewPageContext } from "../../feature";
import { ErrorComponent, LoadingComponent } from "@app/components";
import ProductViewSlideComponent from "./ProductViewSlide.component";
import ProductViewSocialComponent from "./ProductViewSocial.component";
import ProductViewTopComponent from "./ProductViewTop.component";
import ProductViewInforComponent from "./ProductViewInfor.component";

const ProductViewContainer = ({ slug }: { slug: string }) => {
  const history = useNavigate();

  const { productLoadingStatus, productData, productError } = useContext(
    ProductViewPageContext
  );

  if (productLoadingStatus === "loading") {
    return <LoadingComponent />;
  }

  if (productLoadingStatus === "error") {
    return <ErrorComponent error={productError} />;
  }

  if (!productData) {
    history("/products");
    return <></>;
  }

  return (
    <div className="products-view">
      <ProductViewTopComponent />

      <div className="container">
        <div className="products-view__content">
          <ProductViewSocialComponent
            likeNumber={0}
            shareNumber={0}
            twitterNumber={0}
          />
          <ProductViewInforComponent productData={productData} />

          <ProductViewSlideComponent slug={slug} />
        </div>
      </div>
    </div>
  );
};

export default ProductViewContainer;
