import React from "react";
import { raw } from "@app/utils";
import { NoDataComponent } from "@app/components";

const ProductViewManualGuideComponent = ({ guide }: { guide: string }) => {
  if (!guide) {
    return (
      <div className="products-view__wrapper-bottom">
        <NoDataComponent />
      </div>
    );
  }

  return <div id="raw-html" className="products-view__wrapper-bottom">{raw(guide)}</div>;
};

export default ProductViewManualGuideComponent;
