import React from "react";
import { imgLinktoServer } from "@app/utils";
import { Autoplay, Navigation, Pagination } from "swiper";
import { Swiper, SwiperSlide } from "swiper/react";
import { NoDataComponent } from "@app/components";

const ProductViewBacktestComponent = ({
  backtestImages,
}: {
  backtestImages: string | null;
}) => {
  if (!backtestImages || !JSON.parse(backtestImages)) {
    return (
      <div className="products-view__backtest">
        <NoDataComponent />
      </div>
    );
  }

  const imagesBacktest: string[] = JSON.parse(backtestImages);

  return (
    <div className="products-view__backtest">
      <Swiper
        grabCursor={true}
        navigation={true}
        pagination={{ clickable: true }}
        modules={[Navigation, Autoplay, Pagination]}
        className="products-view__backtest-swiper"
        loop={true}
        slidesPerView={1}
      >
        {imagesBacktest.map((item, index) => {
          return (
            <SwiperSlide
              key={index}
              className="products-view__backtest-swiper-item"
            >
              <img src={`${imgLinktoServer}${item}`} alt="" />
            </SwiperSlide>
          );
        })}
      </Swiper>
      <div></div>
    </div>
  );
};

export default ProductViewBacktestComponent;
