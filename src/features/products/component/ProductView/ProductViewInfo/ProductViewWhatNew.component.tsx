import React from "react";
import { raw } from "@app/utils";
import { NoDataComponent } from "@app/components";

const ProductViewWhatNewComponent = ({ version }: { version: string }) => {
  if (!version) {
    return (
      <div className="products-view__wrapper-bottom">
        <NoDataComponent />
      </div>
    );
  }

  return <div id="raw-html" className="products-view__wrapper-bottom">{raw(version)}</div>;
};

export default ProductViewWhatNewComponent;
