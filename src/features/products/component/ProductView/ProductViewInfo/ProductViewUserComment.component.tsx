import React from "react";
import { raw } from "@app/utils";
import { NoDataComponent } from "@app/components";

const ProductViewUserCommentComponent = ({ comment }: { comment: string }) => {
  if (!comment) {
    return (
      <div className="products-view__wrapper-bottom">
        <NoDataComponent />
      </div>
    );
  }

  return <div id="raw-html" className="products-view__wrapper-bottom">{raw(comment)}</div>;
};

export default ProductViewUserCommentComponent;
