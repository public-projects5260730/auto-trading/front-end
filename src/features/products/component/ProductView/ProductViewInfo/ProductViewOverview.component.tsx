import React from "react";
import { raw } from "@app/utils";
import { NoDataComponent } from "@app/components";

const ProductViewOverViewComponent = ({ content }: { content: string }) => {
  if (!content) {
    return (
      <div className="products-view__overview-content">
        <NoDataComponent />
      </div>
    );
  }

  return <div id="raw-html" className="products-view__overview-content">{raw(content)}</div>;
};

export default ProductViewOverViewComponent;
