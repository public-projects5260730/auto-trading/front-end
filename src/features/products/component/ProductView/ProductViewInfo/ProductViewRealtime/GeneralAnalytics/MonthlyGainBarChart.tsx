import React from "react";
import HighchartsReact from 'highcharts-react-official';
import Highcharts from 'highcharts';

const MonthlyGainBarChart = ({
  title,
  chartData,
}: {
  title: string;
  chartData: Record<number, number>;
}) => {
  const seriesData = Object.values(chartData);
  if (!seriesData || !seriesData.length) {
    return <></>;
  }
  const categories = Object.keys(chartData).map(
    (cate) => parseFloat(cate) * 1000,
  );
  const options = {
    title: {
      text: title,
    },
    tooltip: {
      pointFormat: '<span>{point.y}</span>',
      valueDecimals: 2,
      valueSuffix: ' %',
      headerFormat: '',
    },
    yAxis: {
      title: { text: '' },
      stackLabels: {
        enabled: true,
        format: '{total:.2f} %',
        style: {
          fontWeight: 'bold',
        },
      },
    },
    xAxis: {
      type: 'datetime',
      categories: categories,
      labels: {
        format: '{value:%m}',
      },
    },
    legend: {
      enabled: false,
    },
    plotOptions: { column: { stacking: 'normal', maxPointWidth: 60 } },
    exporting: { enabled: false },
    series: [
      {
        name: '',
        type: 'column',
        data: seriesData,

      },
    ],
    responsive: {
      rules: [
        {
          condition: {
            maxWidth: 500,
          },
          chartOptions: {
            legend: {
              align: 'center',
              verticalAlign: 'bottom',
              layout: 'horizontal',
            },
          },
        },
      ],
    },
    credits: {
      enabled: false,
    },
  };

  return (
    <HighchartsReact highcharts={Highcharts} options={options} />
  );
};

export default React.memo(MonthlyGainBarChart);
