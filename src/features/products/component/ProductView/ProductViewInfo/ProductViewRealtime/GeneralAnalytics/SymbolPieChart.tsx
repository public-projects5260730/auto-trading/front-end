import React from "react";
import HighchartsReact from 'highcharts-react-official';
import Highcharts from 'highcharts';
import { ISymbolCollection } from "@app/types";

const SymbolPieChart = ({
  title,
  chartData,
}: {
  title: string;
  chartData: ISymbolCollection[];
}) => {
  if (!chartData?.length) {
    return <></>;
  }
  const seriesData = chartData.map(({ symbol, countSymbol }) => ({ name: symbol?.toUpperCase(), y: +countSymbol }));

  const options = {
    title: {
      text: title,
    },
    series: [
      {
        type: 'pie',
        name: '',
        data: seriesData,
      },
    ],
    tooltip: {
      pointFormat: '<b>{point.name}</b>: {point.percentage:.2f} %',
    },
    legend: {
      useHTML: true,
      labelFormat: '<span class="legend-title">{name}</span>',
    },
    plotOptions: {
      pie: {
        allowPointSelect: true,
        cursor: 'pointer',
        dataLabels: {
          enabled: false,
        },
        showInLegend: true,
      },
    },
    exporting: { enabled: false },
    credits: {
      enabled: false,
    },
  };

  return (
    <HighchartsReact highcharts={Highcharts} options={options} />
  );
};

export default React.memo(SymbolPieChart);
