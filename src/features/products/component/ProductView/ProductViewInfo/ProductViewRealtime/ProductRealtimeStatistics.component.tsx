import { useContext } from "react";
import { ProductViewPageContext } from "@app/features/products/feature";
import { NoDataComponent } from "@app/components";
import { useTrans } from "@app/hooks";
import { formatCurrency, formatDate, formatNumber } from "@app/utils";
import moment from "moment";

const ProductRealtimeStatisticsComponent = () => {
  const trans = useTrans();
  const { productData } = useContext(ProductViewPageContext);

  const stats = productData?.account_live?.stats;
  if (!stats) {
    return <NoDataComponent />;
  }

  return (
    <div className="product-statistics">
      <ul className="list-group list-group-flush grid-repeat-fill-35x w-100 grid-order-2-column">
        <li className="list-group-item list-group-item-action grid-max-1fr">
          <strong className="text-capitalize">{trans("Trades")}:</strong>
          <div className="text-right">{formatNumber(stats?.total_trades)}</div>
        </li>

        <li className="list-group-item list-group-item-action grid-max-1fr">
          <strong className="text-capitalize">{trans("best_trade")}:</strong>
          <div className="text-right">
            {!!stats?.time_best_profit && (
              <span className="small mr-2">
                ({formatDate(stats?.time_best_profit)})
              </span>
            )}
            {formatCurrency(stats?.best_profit)}
          </div>
        </li>

        <li className="list-group-item list-group-item-action grid-max-1fr">
          <strong className="text-capitalize">{trans("worst_trade")}:</strong>
          <div className="text-right">
            {!!stats?.time_worst_profit && (
              <span className="small mr-2">
                ({formatDate(stats?.time_worst_profit)})
              </span>
            )}
            {formatCurrency(stats?.worst_profit)}
          </div>
        </li>

        <li className="list-group-item list-group-item-action grid-max-1fr">
          <strong className="text-capitalize">
            {trans("best_trade")}({trans("pips")}):
          </strong>
          <div className="text-right">
            {!!stats?.time_best_pips && (
              <span className="small mr-2">
                ({formatDate(stats?.time_best_pips)})
              </span>
            )}
            {formatNumber(stats?.best_pips)}
          </div>
        </li>

        <li className="list-group-item list-group-item-action grid-max-1fr">
          <strong className="text-capitalize">
            {trans("worst_trade")}({trans("pips")}):
          </strong>
          <div className="text-right">
            {!!stats?.time_worst_pips && (
              <span className="small mr-2">
                ({formatDate(stats?.time_worst_pips)})
              </span>
            )}
            {formatNumber(stats?.worst_pips)}
          </div>
        </li>

        <li className="list-group-item list-group-item-action grid-max-1fr">
          <strong className="text-capitalize">{trans("maximun_lot")}:</strong>
          <div className="text-right">{formatNumber(stats?.lot_max)}</div>
        </li>

        <li className="list-group-item list-group-item-action grid-max-1fr">
          <strong className="text-capitalize">{trans("pips")}:</strong>
          <div className="text-right">{formatNumber(stats?.pips_total)}</div>
        </li>

        <li className="list-group-item list-group-item-action grid-max-1fr">
          <strong className="text-capitalize">{trans("commission")}:</strong>
          <div className="text-right">
            {formatNumber(stats?.commission_total)}
          </div>
        </li>

        <li className="list-group-item list-group-item-action grid-max-1fr">
          <strong className="text-capitalize">
            {trans("avg_profit_trade")}:
          </strong>
          <div className="text-right">
            {formatCurrency(stats?.avg_profit_trade)}
          </div>
        </li>

        <li className="list-group-item list-group-item-action grid-max-1fr">
          <strong className="text-capitalize">
            {trans("avg_loss_trade")}:
          </strong>
          <div className="text-right">
            {formatCurrency(stats?.avg_loss_trade)}
          </div>
        </li>

        <li className="list-group-item list-group-item-action grid-max-1fr">
          <strong className="text-capitalize">
            {trans("avg_trade_length")}:
          </strong>
          <div className="text-right">
            {moment.duration(stats?.avg_time_trade * 1000).format()}
          </div>
        </li>

        <li className="list-group-item list-group-item-action grid-max-1fr">
          <strong className="text-capitalize">{trans("longs_won")}:</strong>
          <div className="text-right">
            {`${formatNumber(stats?.buy_win_trades)} / ${formatNumber(
              stats?.buy_trades
            )}`}
          </div>
        </li>

        <li className="list-group-item list-group-item-action grid-max-1fr">
          <strong className="text-capitalize">{trans("shorts_won")}:</strong>
          <div className="text-right">
            {`${formatNumber(stats?.sell_win_trades)} / ${formatNumber(
              stats?.sell_trades
            )}`}
          </div>
        </li>

        <li className="list-group-item list-group-item-action grid-max-1fr">
          <strong className="text-capitalize">{trans("lots")}:</strong>
          <div className="text-right">{formatNumber(stats?.lot_total)}</div>
        </li>

        <li className="list-group-item list-group-item-action grid-max-1fr">
          <strong className="text-capitalize">{trans("minimum_lot")}:</strong>
          <div className="text-right">{formatNumber(stats?.lot_min)}</div>
        </li>

        <li className="list-group-item list-group-item-action grid-max-1fr">
          <strong className="text-capitalize">{trans("profit_factor")}:</strong>
          <div className="text-right">{formatNumber(stats?.profit_factor)}</div>
        </li>

        <li className="list-group-item list-group-item-action grid-max-1fr">
          <strong className="text-capitalize">{trans("swap_interest")}:</strong>
          <div className="text-right">{formatCurrency(stats?.swap_total)}</div>
        </li>
      </ul>
    </div>
  );
};

export default ProductRealtimeStatisticsComponent;
