import React, { useContext } from "react";
import {
  ErrorComponent,
  LoadingComponent,
  NoDataComponent,
  Pagination,
} from "@app/components";
import { useTrans } from "@app/hooks";
import moment from "moment";
import "moment-duration-format";
import { Table } from "react-bootstrap";
import { ProductRealtimeContext } from "@app/features/products/feature";
import { getOrderType } from "@app/utils";

const ProductRealtimeTableComponent = () => {
  const trans = useTrans();

  const { realtimeError, realtimeLoadingStatus, realtimeData } = useContext(
    ProductRealtimeContext
  );

  if (realtimeLoadingStatus === "loading") {
    return <LoadingComponent />;
  }

  if (realtimeLoadingStatus === "error") {
    return <ErrorComponent error={realtimeError} />;
  }

  if (!realtimeData || realtimeData.data.length === 0) {
    return <NoDataComponent />;
  }

  return (
    <>
      <div className="table-responsive">
        <Table
          striped
          hover
          className="products-view__realtime-table table table-hover table-striped text-nowrap text-center"
        >
          <thead>
            <tr>
              <th>{trans("open_date")}</th>
              <th>{trans("close_date")}</th>
              <th>{trans("symbol")}</th>
              <th className="fix-width-100">{trans("order_type")}</th>
              <th>{trans("lots")}</th>
              <th>SL</th>
              <th>TP</th>
              <th className="fix-width-100">{trans("open_price")}</th>
              <th className="fix-width-100">{trans("close_price")}</th>
              <th>{trans("points")}</th>
              <th>{trans("swap")}</th>
              <th>{trans("commission")}</th>
              <th>{trans("profit")}($)</th>
              <th>{trans("duration")}</th>
            </tr>
          </thead>
          <tbody>
            {realtimeData.data.map((item, index) => {
              return (
                <tr key={index}>
                  <td>
                    {moment.unix(item.open_time).format("DD/MM/YYYY HH:mm")}
                  </td>
                  <td>
                    {moment.unix(item.close_time).format("DD/MM/YYYY HH:mm")}
                  </td>
                  <td>{item.symbol}</td>
                  <td
                    className={
                      item.order_type % 2 !== 0 ? "red-color" : "green-color"
                    }
                  >
                    {getOrderType(item.order_type)}
                  </td>
                  <td>{item.lot}</td>
                  <td>{item.sl_price}</td>
                  <td>{item.tp_price}</td>
                  <td>{item.open_price}</td>
                  <td>{item.close_price}</td>
                  <td className={item.pips <= 0 ? "red-color" : "green-color"}>
                    {item.pips}
                  </td>
                  <td className={item.swap <= 0 ? "red-color" : "green-color"}>
                    {item.swap}
                  </td>
                  <td
                    className={
                      item.commission <= 0 ? "red-color" : "green-color"
                    }
                  >
                    {item.commission}
                  </td>
                  <td
                    className={
                      item.profit_gross <= 0 ? "red-color" : "green-color"
                    }
                  >
                    {item.profit_gross} ($)
                  </td>
                  <td>{moment.duration(item.duration).humanize()}</td>
                </tr>
              );
            })}
          </tbody>
        </Table>
      </div>

      <div className="products__pagination">
        <Pagination
          params={{
            page: realtimeData.page,
            perPage: realtimeData.perPage,
            resultCount: realtimeData.resultCount,
            total: realtimeData.total,
          }}
        />
      </div>
    </>
  );
};

export default ProductRealtimeTableComponent;
