import { ProductViewPageContext } from "@app/features/products/feature";
import moment from "moment";
import { useContext, useMemo, useState } from "react";
import { Tab, Tabs } from "react-bootstrap";
import { MonthlyGainBarChart, SymbolPieChart } from "./GeneralAnalytics";
import { useTrans } from "@app/hooks";

const ProductRealtimeGeneralAnalyticsComponent = () => {
  const trans = useTrans();

  const { productData } = useContext(ProductViewPageContext);

  const groupMonthlyGainByYears = useMemo((): Record<string, Record<number, number>> => {
    if (!productData?.account_live?.stats?.gain_collection?.length) {
      return {};
    }
    const monthlyGain = productData.account_live.stats.gain_collection;
    return monthlyGain.reduce((group: any, { time, value }) => {
      const year = moment.unix(+time).year();
      const oldData = group?.[year] || {}
      return { ...group, [year]: { ...oldData, [time]: value } }
    }, {})
  }, [productData?.account_live?.stats?.gain_collection]);

  const [activeKey, setActiveKey] = useState(() => {
    const years = Object.keys(groupMonthlyGainByYears);
    if (!years?.length) {
      return;
    }
    return years[0];
  });


  return <div className="product-view-general-analytics">
    {!!Object.keys(groupMonthlyGainByYears)?.length && (
      <div>
        <Tabs
          defaultActiveKey={activeKey}
          id="monthly-gain-tabs"
          onSelect={(key) => setActiveKey(`${key}`)}
          transition={false}
        >
          {Object.entries(groupMonthlyGainByYears).map(([year, data]) => (
            <Tab
              eventKey={year}
              title={year}
              key={year}
            >
              <MonthlyGainBarChart
                title={trans('monthly_gain_of_year_x', {
                  year: year,
                })}
                chartData={data}
                key={year}
              />
            </Tab>
          ))}
        </Tabs>
      </div>
    )}

    {!!productData?.account_live?.stats?.symbol_collection?.length && (
      <div>
        <SymbolPieChart title={trans('pairs_segmentation')} chartData={productData.account_live.stats.symbol_collection} />
      </div>
    )}
  </div>
  return <div>GeneralAnalytics</div>;
};

export default ProductRealtimeGeneralAnalyticsComponent;
