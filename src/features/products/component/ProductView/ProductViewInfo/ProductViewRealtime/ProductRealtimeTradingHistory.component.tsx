import React, { useContext } from "react";
import { useTrans } from "@app/hooks";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faCalendarAlt } from "@fortawesome/free-solid-svg-icons";
import ReactDatePicker from "react-datepicker";
import ProductRealtimeTableComponent from "./ProductRealtimeTable.component";
import { ProductRealtimeContext } from "@app/features/products/feature";

const ProductRealtimeTradingHistoryComponent = () => {
  const trans = useTrans();
  const {
    fromDate,
    setFromDate,
    toDate,
    setToDate,
    rangeDate,
    handleRangeDate,
  } = useContext(ProductRealtimeContext);

  return (
    <div className="products-view__realtime-history">
      <div className="products-view__realtime-filter">
        <div className="products-view__realtime-filter-item ">
          <FontAwesomeIcon
            icon={faCalendarAlt}
            className="products-view__realtime-filter-icon"
          />
          <select
            onChange={(e) => handleRangeDate(e.target.value)}
            value={rangeDate}
          >
            <option value="">{trans("custom")}</option>
            <option value="today">{trans("day")}</option>
            <option value="this_month">{trans("this_month")}</option>
            <option value="last_month">{trans("last_month")}</option>
          </select>
          <img
            src="/assets/images/down.svg"
            alt="down"
            className="products-view__realtime-filter-custom"
          />
        </div>
        <div className="products-view__realtime-filter-item">
          <div className="products-view__realtime-filter-img">
            <img src="/assets/images/from.svg" alt="arrow left" />
          </div>
          <ReactDatePicker
            onChange={(date: Date) => setFromDate(date)}
            selected={fromDate}
            maxDate={new Date()}
            dateFormat="dd/MM/yyyy"
            isClearable
            placeholderText={trans("from_date") || "From date"}
          />
        </div>
        <div className="products-view__realtime-filter-item">
          <div className="products-view__realtime-filter-img">
            <img src="/assets/images/to.svg" alt="arrow right" />
          </div>
          <ReactDatePicker
            onChange={(date: Date) => setToDate(date)}
            selected={toDate}
            maxDate={new Date()}
            dateFormat="dd/MM/yyyy"
            isClearable
            placeholderText={trans("to_date") || "To date"}
          />
        </div>
      </div>

      <ProductRealtimeTableComponent />
    </div>
  );
};

export default ProductRealtimeTradingHistoryComponent;
