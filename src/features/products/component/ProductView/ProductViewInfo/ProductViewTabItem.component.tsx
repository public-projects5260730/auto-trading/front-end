import { useTrans } from "@app/hooks";
import React, { Dispatch, SetStateAction } from "react";

const ProductViewTabItemComponent = ({
  tabName,
  tabActive,
  setTabActive,
}: {
  tabName: string;
  tabActive: string;
  setTabActive: Dispatch<SetStateAction<string>>;
}) => {
  const trans = useTrans();

  return (
    <li
      className={`products-view__info-list-item ${
        tabActive === tabName ? "active" : ""
      }`}
      onClick={() => setTabActive(tabName)}
    >
      {trans(tabName)}
    </li>
  );
};

export default ProductViewTabItemComponent;
