import React, { useContext } from "react";
import {
  ErrorComponent,
  LoadingComponent,
  NoDataComponent,
} from "@app/components";
import { ProductViewPageContext } from "@app/features/products/feature";
import ProductViewFQAItemComponent from "./ProductViewFQAItem.component";

const ProductViewFQAComponent = () => {
  const { fAQLoadingStatus, fAQData, fAQError } = useContext(
    ProductViewPageContext
  );

  if (fAQLoadingStatus === "loading") {
    return (
      <div className="products-view__wrapper-bottom">
        <LoadingComponent />
      </div>
    );
  }

  if (fAQLoadingStatus === "error") {
    return (
      <div className="products-view__wrapper-bottom">
        <ErrorComponent error={fAQError} />
      </div>
    );
  }

  if (!fAQData || fAQData.length === 0) {
    return (
      <div className="products-view__wrapper-bottom">
        <NoDataComponent />
      </div>
    );
  }

  return (
    <div className="products-view__wrapper-bottom">
      {fAQData.map((item, index) => (
        <ProductViewFQAItemComponent key={index} item={item} />
      ))}
    </div>
  );
};

export default ProductViewFQAComponent;
