import React, { useContext } from "react";
import { ProductChartItemComponent } from "@app/components";
import ProductViewTabItemComponent from "./ProductViewTabItem.component";
import { IProducts } from "@app/types";
import {
  ProductRealtimeGeneralAnalyticsComponent,
  ProductRealtimeStatisticsComponent,
  ProductRealtimeTradingHistoryComponent,
} from "./ProductViewRealtime";
import { ProductRealtimeContext } from "@app/features/products/feature";

const ProductViewRealtimeComponent = ({
  productData,
}: {
  productData: IProducts;
}) => {
  const { tabRealtimeActive, setTabRealtimeActive } = useContext(
    ProductRealtimeContext
  );

  return (
    <div className="products-view__realtime">
      <ProductChartItemComponent botData={productData} />

      <div className="products-view__wrapper-bottom">
        <ul className="products-view__info-list">
          {/* <ProductViewTabItemComponent
            setTabActive={setTabRealtimeActive}
            tabActive={tabRealtimeActive}
            tabName="account"
          /> */}
          <ProductViewTabItemComponent
            setTabActive={setTabRealtimeActive}
            tabActive={tabRealtimeActive}
            tabName="trading_history"
          />
          <ProductViewTabItemComponent
            setTabActive={setTabRealtimeActive}
            tabActive={tabRealtimeActive}
            tabName="statistics"
          />
          <ProductViewTabItemComponent
            setTabActive={setTabRealtimeActive}
            tabActive={tabRealtimeActive}
            tabName="general_analytics"
          />
        </ul>

        {/* {tabRealtimeActive === "account" && <ProductRealtimeAccountComponent />} */}
        {tabRealtimeActive === "trading_history" && (
          <ProductRealtimeTradingHistoryComponent />
        )}

        {tabRealtimeActive === "statistics" && (
          <ProductRealtimeStatisticsComponent />
        )}

        {tabRealtimeActive === "general_analytics" && (
          <ProductRealtimeGeneralAnalyticsComponent />
        )}
      </div>
    </div>
  );
};

export default ProductViewRealtimeComponent;
