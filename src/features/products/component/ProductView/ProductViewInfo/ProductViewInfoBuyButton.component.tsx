import { useState, useContext } from "react";
import { useTrans } from "@app/hooks";
import { Link } from "react-router-dom";
import { PurchaseLicenseProductModal } from "@app/features/payments";
import { ProductViewPageContext } from "@app/features/products/feature";

const ProductViewInfoBuyButtonComponent = ({
  imgLink,
  imgAlt,
  text,
  value,
  classCustom,
  link,
}: {
  imgLink: string;
  imgAlt: string;
  text: string;
  value: string;
  classCustom?: string;
  link: string;
}) => {
  const trans = useTrans();

  const { productData } = useContext(ProductViewPageContext);
  const [isShowModal, setIsShowModal] = useState(false);

  return (
    <>
      <Link to={link}>
        <button
          className={`products-view__overview-buy-item ${
            classCustom ? classCustom : ""
          }`}
          onClick={() => setIsShowModal(!isShowModal)}
        >
          <div className="products-view__overview-buy-img">
            <img src={imgLink} alt={imgAlt} />
          </div>
          <div className="products-view__overview-buy-text">
            <h5>{trans(text)}</h5>
            <p>{value}</p>
          </div>
        </button>
      </Link>
      <PurchaseLicenseProductModal
        product={productData}
        isShow={isShowModal}
        onHide={() => setIsShowModal(false)}
      />
    </>
  );
};

export default ProductViewInfoBuyButtonComponent;
