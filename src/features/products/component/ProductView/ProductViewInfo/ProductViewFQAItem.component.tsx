import { useLanguage } from "@app/hooks";
import { IFAQ } from "@app/types/FQA";
import { raw } from "@app/utils";
import { faMinus, faPlus } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { useState } from "react";
import { Collapse } from "react-bootstrap";

const ProductViewFQAItemComponent = ({ item }: { item: IFAQ }) => {
  const [isShow, setIsShow] = useState(false);
  const { title: question, content: answer } = useLanguage<IFAQ>(item);

  return (
    <div className="products-view__FQA">
      <div
        className="products-view__FQA-question"
        onClick={() => setIsShow(!isShow)}
        aria-controls={`faq-collapse--${item.id}`}
        aria-expanded={isShow}
      >
        <FontAwesomeIcon
          icon={isShow ? faMinus : faPlus}
          className="products-view__FQA-question-icon"
        />
        <p>{question}</p>
      </div>
      <Collapse in={isShow}>
        <div id={`faq-collapse--${item.id}`}>
          <div
            id="raw-html"
            className={`products-view__FQA-answer ${
              isShow ? "show-content" : ""
            }`}
          >
            {raw(answer)}
          </div>
        </div>
      </Collapse>
    </div>
  );
};

export default ProductViewFQAItemComponent;
