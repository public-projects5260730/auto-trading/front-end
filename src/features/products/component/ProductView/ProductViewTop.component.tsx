import React, { useContext, useState } from "react";
import { useHeader, useLanguage, useTrans } from "@app/hooks";
import { ProductViewPageContext } from "../../feature";
import { PurchaseLicenseProductModal } from "@app/features/payments";
import { IProducts } from "@app/types";
import { imageUrl } from "@app/utils";
import TrialLicenseProductModal from "@app/features/payments/TrialLicenseProductModal";

const ProductViewTopComponent = () => {
  const trans = useTrans();

  const { productData } = useContext(ProductViewPageContext);
  const [showModal, setShowModal] = useState<"buy" | "trial">();

  const productLanguage = useLanguage<IProducts>(productData);

  useHeader({
    pageTitle: productLanguage?.title || "",
    breadcrumbs: [
      {
        title: productLanguage?.title || "",
        active: true,
      },
    ],
  });

  return (
    <div className="products-view__top">
      <div className="products-view__top-wrapper">
        <div className="container">
          <div className="products-view__top-container">
            <div className="products-view__top-avatar">
              <img
                src={imageUrl(productData?.image)}
                alt={productLanguage?.title}
              />
            </div>
            <div className="products-view__top-info">
              <h3>{productLanguage?.title}</h3>
              <ul className="products-view__top-list">
                <li
                  className="products-view__top-list-item buy"
                  onClick={() => setShowModal("buy")}
                >
                  {trans("buy")}
                </li>
                <li
                  className="products-view__top-list-item free-trial"
                  onClick={() => setShowModal("trial")}
                >
                  {trans("product.free_trial")}
                </li>
                {/* <li
                  className="products-view__top-list-item rent"
                  onClick={() => setIsShowModal(!isShowModal)}
                >
                  {trans("product.rent")}
                </li>
                <li
                  className="products-view__top-list-item free"
                  onClick={() => setIsShowModal(!isShowModal)}
                >
                  {trans("product.get_free")}
                </li> */}
              </ul>
              <PurchaseLicenseProductModal
                product={productData}
                isShow={showModal == "buy"}
                onHide={() => setShowModal(undefined)}
              />
              <TrialLicenseProductModal
                product={productData}
                isShow={showModal == "trial"}
                onHide={() => setShowModal(undefined)}
              />
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default ProductViewTopComponent;
