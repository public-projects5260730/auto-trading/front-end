import {
  faArrowAltCircleUp,
  faMagnifyingGlass,
} from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { useTrans } from "@app/hooks";

const ProductSearchComponent = () => {
  const trans = useTrans();

  return (
    <div className="products__search">
      <div className="products__search-text">
        <FontAwesomeIcon
          icon={faArrowAltCircleUp}
          className="products__search-text-icon"
        />
        <p>{trans("product.discover-experts")}</p>
      </div>

      <div className="products__search-input">
        <input placeholder={trans("search").toString() || "Search"} />
        <FontAwesomeIcon
          icon={faMagnifyingGlass}
          className="products__search-input-icon"
        />
      </div>
    </div>
  );
};

export default ProductSearchComponent;
