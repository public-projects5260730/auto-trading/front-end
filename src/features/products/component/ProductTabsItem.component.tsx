import React, { Dispatch, SetStateAction } from "react";
import { useTrans } from "@app/hooks";
import { Link } from "react-router-dom";

const ProductTabsItemComponent = ({
  tabLink,
  tabName,
  tabActive,
  setTabActive,
}: {
  tabLink: string;
  tabName: string;
  tabActive: string;
  setTabActive: Dispatch<SetStateAction<string>>;
}) => {
  const trans = useTrans();

  return (
    <li
      className={`products-tabs__list-item ${
        tabActive === tabName ? "active" : ""
      }`}
      onClick={() => setTabActive(tabName)}
    >
      <Link to={tabLink} className="products-tabs__list-item-link">
        {trans(tabName)}
      </Link>
    </li>
  );
};

export default ProductTabsItemComponent;
