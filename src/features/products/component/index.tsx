export * from "./ProductList";
export * from "./ProductView";
export { default as ProductTabsItemComponent } from "./ProductTabsItem.component";
