import { useEffect } from "react";
import {
  faArrowAltCircleUp,
  faMagnifyingGlass,
} from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { useFilters, useTrans } from "@app/hooks";
import { FormProvider, useForm } from "react-hook-form";

const ProductSearchComponent = () => {
  const trans = useTrans();

  const { filters, pushFilters } = useFilters();

  const methods = useForm<Record<string, unknown>>({
    defaultValues: {
      ...filters,
    },
  });

  const { handleSubmit, register, setValue } = methods;
  const onSubmit = handleSubmit(async (data) => {
    pushFilters({ ...data, page: undefined });
  });

  useEffect(() => {
    if (!filters?.search) {
      setValue("search", "");
    }
  }, [filters.search, setValue]);

  return (
    <FormProvider {...methods}>
      <form onSubmit={onSubmit} className="search">
        <div className="products__search">
          <div className="products__search-text">
            <FontAwesomeIcon
              icon={faArrowAltCircleUp}
              className="products__search-text-icon"
            />
            <p>{trans("product.discover_experts")}</p>
          </div>

          <div className="products__search-input">
            <input placeholder={`${trans("search")}`} {...register("search")} />

            <button type="submit">
              <FontAwesomeIcon
                icon={faMagnifyingGlass}
                className="products__search-input-icon"
              />
            </button>
          </div>
        </div>
      </form>
    </FormProvider>
  );
};

export default ProductSearchComponent;
