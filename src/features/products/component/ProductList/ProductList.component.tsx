import React, { useContext } from "react";
import ProductListItemComponent from "./ProductListItem.component";
import { ProductPageContext } from "../../feature";
import {
  ErrorComponent,
  LoadingComponent,
  NoDataComponent,
  Pagination,
} from "@app/components";

const ProductListComponent = () => {
  const { loadingStatus, error, paginatorData } =
    useContext(ProductPageContext);

  if (loadingStatus === "loading") {
    return <LoadingComponent />;
  }

  if (loadingStatus === "error") {
    return <ErrorComponent error={error} />;
  }

  if (!paginatorData || paginatorData.data.length === 0) {
    return <NoDataComponent />;
  }

  return (
    <div>
      <div className="products-grid__block">
        {paginatorData.data.map((item, index) => {
          return <ProductListItemComponent botsData={item} key={index} />;
        })}
      </div>

      <div className="products__pagination">
        <Pagination
          params={{
            page: paginatorData.page,
            perPage: paginatorData.perPage,
            resultCount: paginatorData.resultCount,
            total: paginatorData.total,
          }}
        />
      </div>
    </div>
  );
};

export default ProductListComponent;
