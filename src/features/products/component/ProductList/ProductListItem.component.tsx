import { Link } from "react-router-dom";
import { useLanguage, useTrans } from "@app/hooks";
import { IProducts } from "@app/types";
import { formatNumber, imgLinktoServer } from "@app/utils";

const ProductListItemComponent = ({ botsData }: { botsData: IProducts }) => {
  const trans = useTrans();
  const productLanguage = useLanguage<IProducts>(botsData);

  return (
    <div className="products-grid__block-item">
      <div className="products-grid__item-info">
        <div className="products-grid__item-info-logo">
          <div className="products-item-info__img">
            <img
              src={`${imgLinktoServer}${botsData.image}`}
              alt="TimeTrading Logo"
            />
          </div>
          <div className="products-item-info__text">
            <Link
              to={`/products/${botsData.slug}/view`}
              className="products-item-info__text-link"
            >
              {productLanguage.title}
            </Link>
            <p>{productLanguage.excerpt}</p>
          </div>
        </div>

        {/* <FontAwesomeIcon icon={faStar} className="products-grid__item-icon" /> */}
      </div>

      <div className="products-grid__item-value">
        <div className="products-value__infor">
          <div className="row">
            <div className="col-6">
              <div className="products-value__infor-item-top">
                <h4>
                  {formatNumber(botsData?.account_live?.stats?.profit_rate)}%
                </h4>
                <p>{trans("product.growth")}</p>
              </div>
            </div>
            <div className="col-6">
              <div className="products-value__infor-item-top">
                <h4>
                  {formatNumber(botsData?.account_live?.stats?.profit_net)}
                </h4>
                <p>{trans("product.profit")}</p>
              </div>
            </div>
          </div>
          <div className="row">
            <div className="col-6">
              <div className="products-value__infor-item-bottom">
                <h4>
                  {formatNumber(botsData?.account_live?.stats?.max_drawdown)}%
                </h4>
                <p>MDD</p>
              </div>
            </div>
            <div className="col-6">
              <div className="products-value__infor-item-bottom">
                <h4>
                  {formatNumber(botsData?.account_live?.stats?.win_rate)}%
                </h4>
                <p>{trans("winrate")}</p>
              </div>
            </div>
          </div>
        </div>

        <div className="products-value__buy">
          <p className="products-value__buy-text">
            {trans("product.download")}:{"  "}
            <strong>10</strong>
          </p>
          <Link to={`/products/${botsData.slug}/view`}>
            <button>{trans("detail")}</button>
          </Link>
        </div>
      </div>
    </div>
  );
};

export default ProductListItemComponent;
