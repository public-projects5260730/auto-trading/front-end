import React from "react";
import { MainTopComponent } from "@app/components";
import {
  ProductListComponent,
  ProductSearchComponent,
  ProductTabsItemComponent,
} from "./component";
import { ProductPageContext, useProductPageData } from "./feature";
import { useHeader, useTrans } from "@app/hooks";

const ProductsPage = () => {
  const trans = useTrans();
  const pageData = useProductPageData();

  useHeader({
    pageTitle: trans("products").toString(),
    breadcrumbs: [
      {
        title: trans("products"),
        active: true,
      },
    ],
  });

  return (
    <ProductPageContext.Provider value={pageData}>
      <div className="products">
        <MainTopComponent />

        <div className="container">
          <div className="products__wrapper">
            <ProductSearchComponent />

            <div className="products__content">
              <ul className="products__tabs-list">
                <ProductTabsItemComponent
                  tabLink="/products"
                  tabName="growth"
                  tabActive={pageData.tabActive}
                  setTabActive={pageData.setTabActive}
                />
                <ProductTabsItemComponent
                  tabLink="/products/maximum"
                  tabName="maximum_drawdown"
                  tabActive={pageData.tabActive}
                  setTabActive={pageData.setTabActive}
                />
                <ProductTabsItemComponent
                  tabLink="/products/profit"
                  tabName="profit"
                  tabActive={pageData.tabActive}
                  setTabActive={pageData.setTabActive}
                />
                <ProductTabsItemComponent
                  tabLink="/products/winrate"
                  tabName="winrate"
                  tabActive={pageData.tabActive}
                  setTabActive={pageData.setTabActive}
                />
                <ProductTabsItemComponent
                  tabLink="/products/top-download"
                  tabName="top_download"
                  tabActive={pageData.tabActive}
                  setTabActive={pageData.setTabActive}
                />
              </ul>

              <ProductListComponent />
            </div>
          </div>
        </div>
      </div>
    </ProductPageContext.Provider>
  );
};

export default ProductsPage;
