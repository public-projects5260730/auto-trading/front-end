import { useAppSelector, useLanguage, useTrans } from "@app/hooks";
import { Link } from "react-router-dom";
import { LoadingComponent } from "@app/components";
import {
  ILicense,
  IPaymentGateway,
  IProducts,
  IPurchaseLicenseFormData,
  ISelectOptions,
} from "@app/types";
import { useEffect, useState, useMemo } from "react";
import { from } from "rxjs";
import { productApi } from "@app/apis";
import { FormProvider, useForm } from "react-hook-form";
import Form from "@app/components/Form";
import { formatNumber } from "@app/utils";
import { selectVisitor } from "@app/store/auth";

const PurchaseLicenseProductForm = ({
  product,
  type,
}: {
  product?: IProducts;
  type?: string;
}) => {
  const trans = useTrans();
  const visitor = useAppSelector(selectVisitor);

  const [licenses, setLicenses] = useState<ILicense[]>();
  const [paymentGateways, setPaymentGateways] = useState<IPaymentGateway[]>();
  const [paymentProfileId, setPaymentProfileId] = useState<number>(0);

  const languageLicenses = useLanguage<ILicense[]>(licenses);

  useEffect(() => {
    if (!product?.slug?.length) {
      setLicenses([]);
      setPaymentGateways([]);
      return;
    }
    const slug = product?.slug;
    const subscriptionLicense = from(productApi.getLicenses(slug)).subscribe(
      ({ licenses }) => {
        setLicenses(licenses || []);
      }
    );

    const subscriptionPayments = from(
      productApi.getPaymentGateways(slug)
    ).subscribe(({ paymentGateways }) => {
      setPaymentGateways(paymentGateways || []);
    });

    return () => {
      subscriptionLicense.unsubscribe();
      subscriptionPayments.unsubscribe();
    };
  }, [product?.slug]);

  const licenseOptions = useMemo((): ISelectOptions[] => {
    if (!languageLicenses?.length) {
      return [];
    }

    return languageLicenses.map(({ id, title }) => ({
      value: id,
      label: title,
    }));
  }, [languageLicenses]);

  const [isSubmitting, setIsSubmitting] = useState(false);
  const [saveError, setSaveError] = useState("");

  const methods = useForm<IPurchaseLicenseFormData>();
  const { handleSubmit, setError, watch } = methods;

  const chooseLicenseId = watch("license_id", 0);

  const chooseLicense = useMemo((): ILicense | undefined => {
    if (!licenses?.length || !chooseLicenseId) {
      return;
    }

    return licenses?.find(({ id }) => id === chooseLicenseId);
  }, [licenses, chooseLicenseId]);

  const onSubmit = handleSubmit((formData: IPurchaseLicenseFormData) => {
    if (isSubmitting || !product?.slug?.length) {
      return;
    }
    if (!paymentProfileId) {
      setSaveError(trans("please_choose_payment_gateway").toString());
      return;
    }

    setSaveError("");
    setIsSubmitting(true);

    productApi
      .sendPurchaseLicense(product.slug, {
        ...formData,
        payment_profile_id: paymentProfileId,
      })
      .then(({ redirect, errors, error }) => {
        setIsSubmitting(false);
        if (error) {
          setSaveError(error);
        }
        if (errors) {
          Object.entries(errors).map(([field, message]) => {
            return setError(field as keyof IPurchaseLicenseFormData, {
              type: "manual",
              message,
            });
          });
        }
        if (redirect?.length) {
          window.location.replace(redirect);
        }
      });
  });

  if (licenses === null || paymentGateways === null) {
    return <LoadingComponent />;
  }

  return (
    <FormProvider {...methods}>
      <form onSubmit={onSubmit}>
        {saveError?.length > 0 && (
          <div className="callout callout-danger text-danger p-2 small">
            {saveError}
          </div>
        )}
        <div className="payment-content">
          <div className="payment-content__info">
            {!visitor?.id && (
              <Form.Row
                name="email"
                label={trans("email").toString()}
                className="col-sm-2"
              >
                <Form.InputGroup name="email">
                  <Form.InputBox
                    type="email"
                    name="email"
                    disabled={isSubmitting}
                    isRequired
                  />
                </Form.InputGroup>
              </Form.Row>
            )}

            <h5>{trans("choose_lisence")}</h5>
            <div className="payment-content__info-lisence">
              <Form.Row
                name="license_id"
                label={trans("lisence").toString()}
                className="col-sm-2 "
              >
                <Form.InputGroup name="license_id">
                  <Form.Select2Box
                    name="license_id"
                    options={licenseOptions}
                    isDisabled={isSubmitting}
                    isRequired
                  />
                </Form.InputGroup>
              </Form.Row>
            </div>

            <div className="row payment-content__info-lisence">
              <div className="col-sm-2">
                <strong>{trans("total_price")}:</strong>
              </div>
              <div className="col">
                <p>
                  {formatNumber(chooseLicense?.cost_amount)}{" "}
                  {chooseLicense?.cost_currency || "$"}
                </p>
              </div>
            </div>
          </div>

          {!!paymentGateways?.length && (
            <div className="payment-content__methods">
              <h5>{trans("choose_payment_methods")}</h5>
              <div className="payment-content__methods-list">
                <ul className="payment__methods-list">
                  {paymentGateways.map((payment) => (
                    <li
                      className={`payment__methods-list-item ${
                        payment.id === paymentProfileId && " active"
                      }`}
                      key={payment.id}
                    >
                      <Link
                        to="#"
                        className="payment__methods-list-link"
                        onClick={(e) => setPaymentProfileId(payment.id)}
                      >
                        <img
                          src={`/assets/images/${payment.provider_id}.svg`}
                          alt={payment.title}
                        />
                      </Link>
                    </li>
                  ))}
                </ul>

                <button className="payment-content__methods-btn">
                  {trans("continue")}
                </button>
              </div>
            </div>
          )}
        </div>
      </form>
    </FormProvider>
  );
};

export default PurchaseLicenseProductForm;
