import { useEffect, useState } from "react";
import { useTrans } from "@app/hooks";
import { IProducts, IUserLicense } from "@app/types";
import { Modal } from "react-bootstrap";
import { from } from "rxjs";
import { productApi } from "@app/apis";
import { LoadingComponent } from "@app/components";
import { apiUrl, formatNumber } from "@app/utils";

const TrialLicenseProductModal = ({
  product,
  type,
  isShow,
  onHide,
}: {
  product?: IProducts;
  type?: string;
  isShow?: boolean;
  onHide?: () => void;
}) => {
  const trans = useTrans();

  const [isSubmitting, setIsSubmitting] = useState(false);
  const [formError, setFormError] = useState("");
  const [userLicense, setUserLicense] = useState<IUserLicense>();

  useEffect(() => {
    if (!isShow || !product?.slug?.length) {
      return;
    }

    setIsSubmitting(true);
    setFormError("");

    const subscription = from(
      productApi.sendTrialLicense(product.slug)
    ).subscribe(({ userLicense, error }) => {
      setIsSubmitting(false);

      if (error) {
        setFormError(error);
        return;
      }

      if (userLicense) {
        setUserLicense(userLicense);
      }
    });

    return () => {
      subscription.unsubscribe();
    };
  }, [isShow, product?.slug]);

  if (!isShow || !product?.id) {
    return <></>;
  }

  return (
    <Modal show={isShow} onHide={onHide} centered animation={false} size="xl">
      <Modal.Header>
        <Modal.Title className="text-truncate">
          {trans("trial_license", { product: product.title })}
        </Modal.Title>
      </Modal.Header>
      <Modal.Body>
        {!!isSubmitting && <LoadingComponent />}
        {formError?.length > 0 && (
          <div className="callout callout-danger text-danger p-2 small">
            {formError}
          </div>
        )}

        {!!userLicense?.license_key?.length && (
          <div className="callout callout-info">
            <p>{trans("you_have_received_a_trial_license")}</p>
            <p>
              <h4>{trans("license_information")}.</h4>
            </p>
            <ul>
              <li>
                <span className="mr-2">{trans("license_key")}:</span>
                <b>{userLicense.license_key}</b>
              </li>
              <li>
                <span className="mr-2">{trans("number_of_using_days")}:</span>
                <b>
                  {formatNumber(userLicense.license_days)} {trans("days")}
                </b>
              </li>
              <li>
                <span className="mr-2">{trans("account_number")}:</span>
                <b>{formatNumber(userLicense.account_number)}</b>
              </li>
            </ul>

            <div>
              <span>
                {trans("please_click_download_button_to_get_bot_files")}
              </span>
              <a
                className="btn btn-info text-white ml-3"
                download
                href={apiUrl("user-licenses/download", {
                  code: userLicense?.license_key,
                })}
              >
                <i className="fa fa-download mr-2"></i>
                <span>{trans("download")}</span>
              </a>
            </div>
          </div>
        )}
      </Modal.Body>
    </Modal>
  );
};

export default TrialLicenseProductModal;
