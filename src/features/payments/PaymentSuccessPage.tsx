import React from "react";
import { ProductBackLinkComponent } from "@app/components";
import { useTrans } from "@app/hooks";

const PaymentSuccessPage = () => {
  const trans = useTrans();

  return (
    <div id="payments-success">
      <div className="container">
        <div className="payment-content">
          <div className="payment-content__confirm">
            <p>{trans("we_confirm_that")}</p>
            <p>
              {trans("account")}: <strong>abc</strong>
            </p>
            <p>
              {trans("has_just_successfully_purchased_an_ultilimited_product")}{" "}
              4,000 USD
            </p>
            <p>{trans("the_initialization_link_has_been_sent")}</p>

            <strong className="payment__note-thanks">
              {trans("thank_you_so_much")}
            </strong>
          </div>

          <div className="payment-content__text">
            <i className="fa fa-check-circle payment-content__text-icon"></i>
            <h3 className="">{trans("succesfull_payment")}</h3>
          </div>
        </div>

        <ProductBackLinkComponent />
      </div>
    </div>
  );
};

export default PaymentSuccessPage;
