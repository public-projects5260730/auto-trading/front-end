export { default as PaymentPage } from "./PaymentPage";
export { default as PaymentSuccessPage } from "./PaymentSuccessPage";
export { default as PurchaseLicenseProductModal } from "./PurchaseLicenseProductModal";
