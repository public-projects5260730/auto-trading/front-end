import { useTrans } from "@app/hooks";
import { Link } from "react-router-dom";
import { ProductBackLinkComponent } from "@app/components";

const PaymentsPage = () => {
  const trans = useTrans();

  return (
    <div id="payments">
      <div className="container">
        <div className="payment-content">
          <div className="payment-content__info">
            <h5>{trans("choose_lisence")}</h5>
            <div className="row payment-content__info-lisence">
              <div className="col-2">
                <span>{trans("lisence_type")}</span>
              </div>
              <div className="col-10">
                <select>
                  <option>2 user/year</option>
                  <option>3 user/year</option>
                  <option>4 user/year</option>
                  <option>5 user/year</option>
                </select>
              </div>
            </div>
            <div className="row payment-content__info-lisence">
              <div className="col-2">
                <span>{trans("total_price")}</span>
              </div>

              <div className="col-10">
                <p>4000$</p>
              </div>
            </div>
          </div>

          <div className="payment-content__methods">
            <h5>{trans("choose_payment_methods")}</h5>
            <div className="payment-content__methods-list">
              <ul className="payment__methods-list">
                <li className="payment__methods-list-item">
                  <Link to="" className="payment__methods-list-link">
                    <img src="/assets/images/paypal.svg" alt="paypal" />
                  </Link>
                </li>
                <li className="payment__methods-list-item">
                  <Link to="" className="payment__methods-list-link">
                    <img src="/assets/images/visa.svg" alt="visa mastercard" />
                  </Link>
                </li>
                <li className="payment__methods-list">
                  <Link to="" className="payment__methods-list-link">
                    <img src="/assets/images/bitcoin.svg" alt="bitcoin" />
                  </Link>
                </li>
              </ul>

              <Link to="">
                <button className="payment-content__methods-btn">
                  {trans("continue")}
                </button>
              </Link>
            </div>
          </div>
        </div>

        <ProductBackLinkComponent />
      </div>
    </div>
  );
};

export default PaymentsPage;
