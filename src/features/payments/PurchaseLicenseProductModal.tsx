import { useTrans } from "@app/hooks";
import { IProducts } from "@app/types";
import { Modal } from "react-bootstrap";
import PurchaseLicenseProductForm from "./PurchaseLicenseProductForm";

const PurchaseLicenseProductModal = ({
  product,
  type,
  isShow,
  onHide,
}: {
  product?: IProducts;
  type?: string;
  isShow?: boolean;
  onHide?: () => void;
}) => {
  const trans = useTrans();

  if (!isShow || !product?.id) {
    return <></>;
  }

  return (
    <div id="payments">
      <div className="container">
        <Modal
          show={isShow}
          onHide={onHide}
          centered
          animation={false}
          size="xl"
        >
          <Modal.Header>
            <Modal.Title className="text-truncate">
              {trans("purchase_license_x", { product: product.title })}
            </Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <PurchaseLicenseProductForm product={product} type={type} />
          </Modal.Body>
        </Modal>
      </div>
    </div>
  );
};

export default PurchaseLicenseProductModal;
