import React from "react";
import { useHeader, useTrans } from "@app/hooks";
import { Link } from "react-router-dom";

const ContactUsPage = () => {
  const trans = useTrans();
  useHeader({
    pageTitle: trans("contact_us").toString(),
    breadcrumbs: [
      {
        title: trans("contact_us"),
        active: true,
      },
    ],
  });

  return (
    <div className="contact-us">
      <div className="contact-us__top">
        <div className="contact-us__top-background" />
      </div>
      <div className="contact-us__main">
        <div className="container">
          <div className="contact-us__main-content">
            <div className="contact-us__main-message">
              <p>{trans("contact_us_content.message_comment")}</p>
              <p>{trans("contact_us_content.message_offer")}</p>
              <p>{trans("contact_us_content.message_follow")}:</p>
            </div>
            <ul className="contact-us__main-list">
              <li className="contact-us__main-list-item">
                <Link to="/contacts-us" className="contact-us__main-list-link">
                  <img src="/assets/images/email.svg" alt="email icon" />
                  <p>{trans("email")}</p>
                </Link>
              </li>
              <li className="contact-us__main-list-item">
                <Link to="/contacts-us" className="contact-us__main-list-link">
                  <img src="/assets/images/telegram.svg" alt="Telegram icon" />
                  <p>{trans("telegram")}</p>
                </Link>
              </li>
              <li className="contact-us__main-list-item">
                <Link to="/contacts-us" className="contact-us__main-list-link">
                  <img src="/assets/images/skype.svg" alt="Skype icon" />
                  <p>{trans("skype")}</p>
                </Link>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  );
};

export default ContactUsPage;
