import { useTrans } from "@app/hooks";
import { faCaretRight } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { Link } from "react-router-dom";

const HomeInformationComponent = () => {
  const trans = useTrans();

  return (
    <div className="home-infor-grid__block">
      <div className="home-infor-grid__block-item">
        <div className="content">
          <article>
            <img src="/assets/images/Group 122.svg" alt="cogs" />
            <h4>{trans("auto_trading")}</h4>
            <p>{trans("home_content.auto_trading_message")}</p>
            <Link
              to="/auto-trading"
              className="home-infor-grid__block-item-link"
            >
              <p>{trans("read_more")}</p>
              <FontAwesomeIcon
                icon={faCaretRight}
                className="home-infor-grid__block-item-icon"
              />
            </Link>
          </article>
        </div>
      </div>

      <div className="home-infor-grid__block-item">
        <div className="content">
          <article>
            <img src="/assets/images/Vector.svg" alt="cubes" />
            <h4>{trans("products")}</h4>
            <p>{trans("home_content.products_message")}</p>
            <Link to="/products" className="home-infor-grid__block-item-link">
              <p>{trans("read_more")}</p>
              <FontAwesomeIcon
                icon={faCaretRight}
                className="home-infor-grid__block-item-icon"
              />
            </Link>
          </article>
        </div>
      </div>

      <div className="home-infor-grid__block-item">
        <div className="content">
          <article>
            <img src="/assets/images/Group 121.svg" alt="sales" />
            <h4>{trans("services")}</h4>
            <p>{trans("home_content.services_message")}</p>
            <Link
              to="/contacts-us"
              className="home-infor-grid__block-item-link"
            >
              <p>{trans("read_more")}</p>
              <FontAwesomeIcon
                icon={faCaretRight}
                className="home-infor-grid__block-item-icon"
              />
            </Link>
          </article>
        </div>
      </div>
    </div>
  );
};

export default HomeInformationComponent;
