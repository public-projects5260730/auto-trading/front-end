import React, { useContext } from "react";
import { Autoplay, Navigation, Pagination } from "swiper";
import { Swiper, SwiperSlide } from "swiper/react";
import { HomePageContext } from "../feature";
import {
  ErrorComponent,
  LoadingComponent,
  NoDataComponent,
  ProductChartItemComponent,
} from "@app/components";

const HomeChartComponent = () => {
  const { productListLoadingStatus, productListError, productsList } =
    useContext(HomePageContext);

  if (productListLoadingStatus === "loading") {
    return <LoadingComponent />;
  }

  if (productListLoadingStatus === "error") {
    return <ErrorComponent error={productListError} />;
  }

  if (!productsList || productsList.length === 0) {
    return <NoDataComponent />;
  }

  return (
    <div className="home-chart-infor__slide-swiper">
      <Swiper
        grabCursor={true}
        navigation={true}
        pagination={{ clickable: true }}
        modules={[Navigation, Autoplay, Pagination]}
        loop={true}
        slidesPerView={1}
      >
        {productsList.map((item, index) => {
          return (
            <SwiperSlide key={index}>
              <ProductChartItemComponent botData={item} />
            </SwiperSlide>
          );
        })}
      </Swiper>
    </div>
  );
};

export default HomeChartComponent;
