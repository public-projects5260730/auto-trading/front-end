import { IProducts } from "@app/types";
import { createContext } from "react";

type HomeContextType = {
  productListLoadingStatus: "loading" | "done" | "error";
  productsList?: IProducts[];
  productListError?: string;
};

export const HomePageContext = createContext<HomeContextType>({
  productListLoadingStatus: "loading",
});
