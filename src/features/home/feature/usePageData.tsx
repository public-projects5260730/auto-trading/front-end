import { useState, useEffect } from "react";
import { concatMap, timer } from "rxjs";
import { productApi } from "@app/apis";
import { IProducts } from "@app/types";
import { intervalDurationTimer } from "@app/utils";

export function usePageData() {
  const [productsList, setProductsList] = useState<IProducts[]>();
  const [productListLoadingStatus, setProductListLoadingStatus] = useState<
    "loading" | "done" | "error"
  >("loading");
  const [productListError, setProductListError] = useState<string>();

  useEffect(() => {
    const subscription = timer(0, intervalDurationTimer())
      .pipe(concatMap(() => productApi.getProductsList(6, { featured: 1 })))
      .subscribe(({ products, error }) => {
        if (products) {
          setProductsList(products);
          setProductListLoadingStatus("done");
        }

        if (error) {
          setProductListLoadingStatus("error");
          setProductListError(error);
        }
      });

    return () => {
      subscription.unsubscribe();
    };
  }, []);

  return {
    productsList,
    productListLoadingStatus,
    productListError,
  };
}
