import React from "react";
import { useHeader, useTrans } from "@app/hooks";
import { HomeChartComponent, HomeInformationComponent } from "./component";
import { MainTopComponent } from "@app/components";
import { HomePageContext, usePageData } from "./feature";

export const HomePage = () => {
  const trans = useTrans();
  const pageData = usePageData();

  useHeader({
    pageTitle: trans("home").toString(),
    section: "main.home",
  });

  return (
    <HomePageContext.Provider value={pageData}>
      <MainTopComponent />

      <div className="home__chart-infor">
        <div className="space_section">
          <div className="container">
            <HomeChartComponent />
            <HomeInformationComponent />
          </div>
        </div>
      </div>
    </HomePageContext.Provider>
  );
};

export default HomePage;
