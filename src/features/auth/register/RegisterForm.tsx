import { useState } from "react";
import { FormProvider, useForm } from "react-hook-form";
import { IRegisterData } from "@app/types";
import { useAppDispatch, useAppSelector, useTrans } from "@app/hooks";
import { Link } from "react-router-dom";
import { authApi } from "@app/apis";
import Form from "@app/components/Form";
import { authActions, selectAccessToken } from "@app/store/auth";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faUserPlus } from "@fortawesome/free-solid-svg-icons";

export const RegisterForm = () => {
  const trans = useTrans();

  const accessToken = useAppSelector(selectAccessToken);
  const dispatch = useAppDispatch();

  const [isSubmitting, setIsSubmitting] = useState(false);
  const [saveError, setSaveError] = useState("");

  const methods = useForm<IRegisterData>();
  const { handleSubmit, setError } = methods;

  const onSubmit = handleSubmit((formData: IRegisterData) => {
    if (isSubmitting) {
      return;
    }
    setSaveError("");
    setIsSubmitting(true);

    authApi.sendRegister(formData).then(({ accessToken, errors, error }) => {
      setIsSubmitting(false);

      if (error) {
        setSaveError(error);
      }
      if (errors) {
        Object.entries(errors).map(([field, message]) => {
          return setError(field as keyof IRegisterData, {
            type: "manual",
            message,
          });
        });
      }
      if (accessToken?.length) {
        dispatch(authActions.setToken(accessToken));
      }
    });
  });

  return (
    <div id="register">
      {accessToken?.length ? (
        <div className="container">
          <div className="login-register">
            <i className="fa fa-check-circle login__top-icon"></i>
            <h3 className="">{trans("registration_was_successful")}</h3>
            <h6>{trans("thanks_for_registering")}</h6>
            <div className="login-register__switch-link">
              <Link to="/" className="login-register__switch-link-link">
                <button>{trans("return_to_the_home_page")}</button>
              </Link>
            </div>
          </div>
        </div>
      ) : (
        <div className="container">
          <div className="login-register">
            <div className="login-register__form">
              <h3>{trans("create_account")}</h3>
              <FormProvider {...methods}>
                <form onSubmit={onSubmit}>
                  {saveError?.length > 0 && (
                    <div className="callout callout-danger text-danger p-2 small">
                      {saveError}
                    </div>
                  )}

                  <div className="login-register__form-input">
                    <Form.Row name="name" isRequired className="col-12">
                      <Form.InputGroup name="name">
                        <Form.InputBox
                          name="name"
                          disabled={isSubmitting}
                          isRequired
                        />
                      </Form.InputGroup>
                    </Form.Row>

                    <Form.Row name="email" isRequired className="col-12">
                      <Form.InputGroup name="email">
                        <Form.InputBox
                          type="email"
                          name="email"
                          disabled={isSubmitting}
                          isRequired
                        />
                      </Form.InputGroup>
                    </Form.Row>

                    <Form.Row name="password" isRequired className="col-12">
                      <Form.InputGroup name="password">
                        <Form.InputBox
                          type="password"
                          name="password"
                          disabled={isSubmitting}
                          isRequired
                        />
                      </Form.InputGroup>
                    </Form.Row>

                    <Form.Row
                      name="password_confirmation"
                      isRequired
                      className="col-12"
                    >
                      <Form.InputGroup name="password_confirmation">
                        <Form.InputBox
                          type="password"
                          name="password_confirmation"
                          disabled={isSubmitting}
                          isRequired
                        />
                      </Form.InputGroup>
                    </Form.Row>
                  </div>

                  <Form.Button
                    isSubmitting={isSubmitting}
                    label={trans("create_account")}
                    icon={<FontAwesomeIcon icon={faUserPlus} />}
                    className="login-register__btn"
                  />
                </form>
              </FormProvider>
            </div>

            <div className="login-register__switch-link">
              <h6>{trans("already_have_an_account")}</h6>
              <Link to="/login" className="login-register__switch-link-link">
                <button>{trans("sign_in")}</button>
              </Link>
            </div>
          </div>
        </div>
      )}
    </div>
  );
};
