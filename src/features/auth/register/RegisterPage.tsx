import { useHeader, useTrans } from "@app/hooks";
import { RegisterForm } from ".";

export const RegisterPage = () => {
  const trans = useTrans();

  useHeader({
    pageTitle: trans("register").toString(),
    section: "register",
    breadcrumbs: [
      {
        title: trans("register"),
        active: true,
      },
    ],
  });

  return <RegisterForm />;
};
