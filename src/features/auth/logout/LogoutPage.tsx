import { useAppDispatch, useReloadPage } from "@app/hooks";
import { authActions } from "@app/store/auth";
import { useEffect } from "react";

export const LogoutPage = () => {
  const dispatch = useAppDispatch();
  const reloadPage = useReloadPage();

  useEffect(() => {
    dispatch(authActions.logout());
    reloadPage("/");
  }, [dispatch, reloadPage]);

  return <></>;
};
