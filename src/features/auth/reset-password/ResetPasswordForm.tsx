import { useState } from "react";
import { FormProvider, useForm } from "react-hook-form";
import { IResetPasswordFormData } from "@app/types";
import { useFilters, useReloadPage, useTrans } from "@app/hooks";
import { Link, useParams } from "react-router-dom";
import { authApi } from "@app/apis";
import Form from "@app/components/Form";
import { ProductBackLinkComponent } from "@app/components";
import { toast } from "react-toastify";

export const ResetPasswordForm = () => {
  const trans = useTrans();

  const { filters } = useFilters();
  const { token } = useParams<{ token: string }>();
  const reloadPage = useReloadPage();

  const [isSubmitting, setIsSubmitting] = useState(false);
  const [saveError, setSaveError] = useState("");

  const methods = useForm<IResetPasswordFormData>({
    defaultValues: {
      token,
      email: filters?.email,
    },
  });
  const { handleSubmit, setError } = methods;

  const onSubmit = handleSubmit((formData: IResetPasswordFormData) => {
    if (isSubmitting) {
      return;
    }
    setSaveError("");
    setIsSubmitting(true);

    authApi
      .sendResetPassword(formData)
      .then(({ successfully, message, errors, error }) => {
        setIsSubmitting(false);
        if (error) {
          setSaveError(error);
        }
        if (errors) {
          Object.entries(errors).map(([field, message]) => {
            return setError(field as keyof IResetPasswordFormData, {
              type: "manual",
              message,
            });
          });
        }
        if (successfully && message) {
          toast.dismiss();
          toast.success(message);
          reloadPage("login");
        }
      });
  });

  return (
    <div id="login">
      <div className="container">
        <div className="login-register">
          <div className="login-register__form">
            <h3>{trans("reset_password")}</h3>
            <FormProvider {...methods}>
              <form onSubmit={onSubmit}>
                {saveError?.length > 0 && (
                  <div className="callout callout-danger text-danger p-2 small">
                    {saveError}
                  </div>
                )}

                <div className="login-register__form-input">
                  <Form.Row name="token" isRequired className="col-12">
                    <Form.InputGroup name="token">
                      <Form.InputBox
                        name="token"
                        disabled={isSubmitting}
                        isRequired
                      />
                    </Form.InputGroup>
                  </Form.Row>

                  <Form.Row name="email" isRequired className="col-12">
                    <Form.InputGroup name="email">
                      <Form.InputBox
                        type="email"
                        name="email"
                        disabled={isSubmitting}
                        isRequired
                      />
                    </Form.InputGroup>
                  </Form.Row>

                  <Form.Row name="password" isRequired className="col-12">
                    <Form.InputGroup name="password">
                      <Form.InputBox
                        type="password"
                        name="password"
                        disabled={isSubmitting}
                        isRequired
                      />
                    </Form.InputGroup>
                  </Form.Row>

                  <Form.Row
                    name="password_confirmation"
                    isRequired
                    className="col-12"
                  >
                    <Form.InputGroup name="password_confirmation">
                      <Form.InputBox
                        type="password"
                        name="password_confirmation"
                        disabled={isSubmitting}
                        isRequired
                      />
                    </Form.InputGroup>
                  </Form.Row>
                </div>

                <Form.Button
                  isSubmitting={isSubmitting}
                  label={trans("reset")}
                  icon={<i className="fa fa-refresh mr-2"></i>}
                  className="login-register__btn"
                />
              </form>
            </FormProvider>
          </div>

          <div className="login-register__switch-link">
            <h6>{trans("already_have_an_account")}</h6>
            <Link to="/login" className="login-register__switch-link-link">
              <button>{trans("sign_in")}</button>
            </Link>
          </div>
        </div>

        <ProductBackLinkComponent />
      </div>
    </div>
  );
};
