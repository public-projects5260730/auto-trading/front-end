import { useHeader, useTrans } from "@app/hooks";
import { ResetPasswordForm } from ".";

export const ResetPasswordPage = () => {
  const trans = useTrans();

  useHeader({
    pageTitle: trans("reset_password").toString(),
    section: "login",
    breadcrumbs: [
      {
        title: trans("login"),
        active: true,
      },
    ],
  });

  return <ResetPasswordForm />;
};
