import { useHeader, useTrans } from "@app/hooks";
import { ForgotPasswordForm } from ".";

export const ForgotPasswordPage = () => {
  const trans = useTrans();

  useHeader({
    pageTitle: trans("forgot_password").toString(),
    section: "login",
    breadcrumbs: [
      {
        title: trans("login"),
        active: true,
      },
    ],
  });

  return <ForgotPasswordForm />;
};
