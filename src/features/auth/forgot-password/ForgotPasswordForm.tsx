import { useState } from "react";
import { FormProvider, useForm } from "react-hook-form";
import { IForgotPasswordFormData } from "@app/types";
import { useTrans } from "@app/hooks";
import { Link } from "react-router-dom";
import { authApi } from "@app/apis";
import Form from "@app/components/Form";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faArrowRightFromBracket } from "@fortawesome/free-solid-svg-icons";
import { ProductBackLinkComponent } from "@app/components";
import { toast } from "react-toastify";

export const ForgotPasswordForm = () => {
  const trans = useTrans();

  const [isSubmitting, setIsSubmitting] = useState(false);
  const [saveError, setSaveError] = useState("");

  const methods = useForm<IForgotPasswordFormData>();
  const { handleSubmit, setError } = methods;

  const onSubmit = handleSubmit((formData: IForgotPasswordFormData) => {
    if (isSubmitting) {
      return;
    }
    setSaveError("");
    setIsSubmitting(true);

    authApi
      .sendForgotPassword(formData)
      .then(({ successfully, message, errors, error }) => {
        setIsSubmitting(false);
        if (error) {
          setSaveError(error);
        }
        if (errors) {
          Object.entries(errors).map(([field, message]) => {
            return setError(field as keyof IForgotPasswordFormData, {
              type: "manual",
              message,
            });
          });
        }
        if (successfully && message) {
          toast.dismiss();
          toast.success(message);
        }
      });
  });

  return (
    <div id="login">
      <div className="container">
        <div className="login-register">
          <div className="login-register__form">
            <h3>{trans("forgotten_password")}</h3>
            <FormProvider {...methods}>
              <form onSubmit={onSubmit}>
                {saveError?.length > 0 && (
                  <div className="callout callout-danger text-danger p-2 small">
                    {saveError}
                  </div>
                )}

                <div className="login-register__form-input">
                  <Form.Row name="email" isRequired className="col-12">
                    <Form.InputGroup name="email">
                      <Form.InputBox
                        type="email"
                        name="email"
                        disabled={isSubmitting}
                        isRequired
                      />
                    </Form.InputGroup>
                  </Form.Row>
                </div>

                <Form.Button
                  isSubmitting={isSubmitting}
                  icon={<FontAwesomeIcon icon={faArrowRightFromBracket} />}
                  className="login-register__btn"
                />
              </form>
            </FormProvider>
          </div>

          <div className="login-register__switch-link">
            <h6>{trans("already_have_an_account")}</h6>
            <Link to="/login" className="login-register__switch-link-link">
              <button>{trans("sign_in")}</button>
            </Link>
          </div>
        </div>

        <ProductBackLinkComponent />
      </div>
    </div>
  );
};
