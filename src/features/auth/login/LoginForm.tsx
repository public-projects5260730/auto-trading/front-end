import { useState } from "react";
import { FormProvider, useForm } from "react-hook-form";
import { ILoginData } from "@app/types";
import { useAppDispatch, useAppSelector, useTrans } from "@app/hooks";
import { Link } from "react-router-dom";
import { authApi } from "@app/apis";
import { authActions, selectAccessToken } from "@app/store/auth";
import Form from "@app/components/Form";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faArrowRightFromBracket } from "@fortawesome/free-solid-svg-icons";
import { ProductBackLinkComponent } from "@app/components";

export const LoginForm = () => {
  const trans = useTrans();

  const accessToken = useAppSelector(selectAccessToken);
  const dispatch = useAppDispatch();

  const [isSubmitting, setIsSubmitting] = useState(false);
  const [saveError, setSaveError] = useState("");

  const methods = useForm<ILoginData>();
  const { handleSubmit, setError, register } = methods;

  const onSubmit = handleSubmit((formData: ILoginData) => {
    if (isSubmitting) {
      return;
    }
    setSaveError("");
    setIsSubmitting(true);

    authApi.sendLogin(formData).then(({ accessToken, errors, error }) => {
      setIsSubmitting(false);
      if (error) {
        setSaveError(error);
      }
      if (errors) {
        Object.entries(errors).map(([field, message]) => {
          return setError(field as keyof ILoginData, {
            type: "manual",
            message,
          });
        });
      }
      if (accessToken?.length) {
        dispatch(authActions.setToken(accessToken));
      }
    });
  });

  return (
    <div id="login">
      {accessToken?.length ? (
        <div className="container">
          <div className="login-register">
            <i className="fa fa-check-circle login__top-icon"></i>
            <h3 className="">{trans("login_successfull")}</h3>
            <div className="login-register__switch-link">
              <Link to="/" className="login-register__switch-link-link">
                <button>{trans("return_to_the_home_page")}</button>
              </Link>
            </div>
          </div>
        </div>
      ) : (
        <div className="container">
          <div className="login-register">
            <div className="login-register__form">
              <h3>{trans("sign_in")}</h3>
              <FormProvider {...methods}>
                <form onSubmit={onSubmit}>
                  {saveError?.length > 0 && (
                    <div className="callout callout-danger text-danger p-2 small">
                      {saveError}
                    </div>
                  )}

                  <div className="login-register__form-input">
                    <Form.Row className="col-12">
                      <Form.InputGroup name="email">
                        <Form.InputBox
                          type="email"
                          name="email"
                          disabled={isSubmitting}
                          isRequired
                        />
                      </Form.InputGroup>
                    </Form.Row>

                    <Form.Row className="col-12">
                      <Form.InputGroup name="password">
                        <Form.InputBox
                          type="password"
                          name="password"
                          disabled={isSubmitting}
                          isRequired
                        />
                      </Form.InputGroup>
                    </Form.Row>
                  </div>

                  <div className="login__remember">
                    <div className="login__remember-left">
                      <input
                        type="checkbox"
                        id="login__remember"
                        {...register("rememberMe")}
                      />
                      <label htmlFor="login__remember">
                        {trans("remember_me")}
                      </label>
                    </div>
                    <p>
                      <Link to="/forgot-password">
                        {trans("forgotten_password")}
                      </Link>
                    </p>
                  </div>

                  <Form.Button
                    isSubmitting={isSubmitting}
                    label={trans("sign_in")}
                    icon={<FontAwesomeIcon icon={faArrowRightFromBracket} />}
                    className="login-register__btn"
                  />
                </form>
              </FormProvider>
            </div>

            <div className="login-register__switch-link">
              <h6>{trans("do_not_have_account")}</h6>
              <Link to="/register" className="login-register__switch-link-link">
                <button>{trans("sign_up")}</button>
              </Link>
            </div>
          </div>

          <ProductBackLinkComponent />
        </div>
      )}
    </div>
  );
};
