import { useHeader, useTrans } from "@app/hooks";
import { LoginForm } from ".";

export const LoginPage = () => {
  const trans = useTrans();

  useHeader({
    pageTitle: trans("login").toString(),
    section: "login",
    breadcrumbs: [
      {
        title: trans("login"),
        active: true,
      },
    ],
  });

  return <LoginForm />;
};
