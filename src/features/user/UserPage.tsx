import React from "react";
import { useAppSelector } from "@app/hooks";
import { selectVisitor } from "@app/store/auth";
import {
  UserProductComponent,
  UserProfileComponent,
  UserProfilePasswordComponent,
  UserTabItemComponent,
} from "./component";
import { NoDataComponent } from "@app/components";
import { useLocation } from "react-router-dom";

const UserPage = () => {
  const { pathname } = useLocation();
  const visitor = useAppSelector(selectVisitor);

  if (!visitor) {
    return <NoDataComponent />;
  }

  return (
    <div id="user-page">
      <div className="container">
        <div className="user__wrapper">
          <div className="user__header">
            <h2>{visitor.name}</h2>
            <ul className="user__header-list">
              <UserTabItemComponent
                tabName="profile"
                tabLink="/user"
                imgAlt="user"
                imgLink="/assets/images/profile.svg"
                pathname={pathname}
              />
              <UserTabItemComponent
                tabName="products"
                tabLink="/user/products"
                imgAlt="cubes"
                imgLink="/assets/images/product.svg"
                pathname={pathname}
              />
              <UserTabItemComponent
                tabName="change_password"
                tabLink="/user/change_password"
                imgAlt="switch"
                imgLink="/assets/images/switch.svg"
                pathname={pathname}
              />
            </ul>
          </div>

          <div className="user__content">
            {pathname === "/user" && <UserProfileComponent visitor={visitor} />}

            {pathname === "/user/products" && (
              <UserProductComponent visitor={visitor} />
            )}

            {pathname === "/user/change_password" && (
              <UserProfilePasswordComponent />
            )}
          </div>
        </div>
      </div>
    </div>
  );
};

export default UserPage;
