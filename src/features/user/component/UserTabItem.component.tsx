import React from "react";
import { useTrans } from "@app/hooks";
import { Link } from "react-router-dom";

const UserTabItemComponent = ({
  imgLink,
  tabName,
  imgAlt,
  pathname,
  tabLink,
  classCustom,
}: {
  imgLink: string;
  tabName: string;
  imgAlt: string;
  tabLink: string;
  pathname: string;
  classCustom?: string;
}) => {
  const trans = useTrans();

  return (
    <li
      className={`user__header-item ${tabLink === pathname ? "active" : ""} ${
        classCustom ? classCustom : ""
      }`}
    >
      <Link to={tabLink} className="user__header-link">
        <img src={imgLink} alt={imgAlt} />
        <p>{trans(tabName)}</p>
      </Link>
    </li>
  );
};

export default UserTabItemComponent;
