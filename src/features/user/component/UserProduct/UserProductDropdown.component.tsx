import { useMemo } from "react";
import { faCog } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { Dropdown, DropdownButton } from "react-bootstrap";
import { useTrans } from "@app/hooks";
import { IUserLicense } from "@app/types";
import { apiUrl } from "@app/utils";

const UserProductDropDownComponent = ({
  userLicense,
}: {
  userLicense: IUserLicense;
}) => {
  const trans = useTrans();
  const permissions = userLicense.permissions;
  const linkDownload = useMemo(
    () => apiUrl("user-licenses/download", { code: userLicense?.license_key }),
    [userLicense?.license_key]
  );

  if (!userLicense?.permissions) {
    return <></>;
  }

  return (
    <DropdownButton
      title={<FontAwesomeIcon icon={faCog} />}
      className="user-products__table-dropdown"
    >
      {!!permissions?.canDownload && (
        <a
          className="user-products__table-dropdown-item dropdown-item"
          download
          href={linkDownload}
        >
          <img src="/assets/images/download.svg" alt="Download button" />
          <p>{trans("download")}</p>
        </a>
      )}
      {!!permissions?.canExtend && (
        <Dropdown.Item className="user-products__table-dropdown-item">
          <img src="/assets/images/upgrade.svg" alt="Upgrade button" />
          <p>{trans("extend")}</p>
        </Dropdown.Item>
      )}
      {!!permissions?.canUpgrade && (
        <Dropdown.Item className="user-products__table-dropdown-item">
          <img src="/assets/images/upgrade.svg" alt="Upgrade button" />
          <p>{trans("upgrade")}</p>
        </Dropdown.Item>
      )}
    </DropdownButton>
  );
};

export default UserProductDropDownComponent;
