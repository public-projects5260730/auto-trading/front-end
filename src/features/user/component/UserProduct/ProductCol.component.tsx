import { useLanguage } from "@app/hooks";
import { IProducts } from "@app/types";
import { Link } from "react-router-dom";

const ProductColComponent = ({ product }: { product?: IProducts }) => {
  const productLanguage = useLanguage<IProducts>(product);
  if (!product?.id) {
    return <>N/A</>;
  }

  return (
    <Link to={`/products/${product.slug}/view`}>{productLanguage?.title}</Link>
  );
};

export default ProductColComponent;
