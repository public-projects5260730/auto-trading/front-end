import { useEffect, useState } from "react";
import { useFilters, useHeader, useTrans } from "@app/hooks";
import UserProductDropDownComponent from "./UserProductDropdown.component";
import { Pagination } from "@app/components";
import { Table } from "react-bootstrap";
import moment from "moment";
import { from } from "rxjs";
import { userLicenseApi } from "@app/apis";
import { IPaginationData, IUser, IUserLicense } from "@app/types";
import { formatDate, formatNumber, protectLicenseKey } from "@app/utils";
import { toast } from "react-toastify";
import ProductColComponent from "./ProductCol.component";

const UserProductComponent = ({ visitor }: { visitor: IUser }) => {
  const trans = useTrans();

  const { filters } = useFilters();

  const [paginate, setPaginate] = useState<IPaginationData<IUserLicense>>();

  useHeader({
    pageTitle: trans("x_products", { user_name: visitor.name }).toString(),
    breadcrumbs: [
      {
        title: trans("x_products", { user_name: visitor.name }),
        active: true,
      },
    ],
  });

  useEffect(() => {
    const subscription = from(
      userLicenseApi.fetchPaginate({ page: filters?.page || 1 })
    ).subscribe(({ paginate }) => {
      if (paginate) {
        setPaginate(paginate);
      }
    });

    return () => {
      subscription.unsubscribe();
    };
  }, [filters?.page]);

  return (
    <div className="user__products">
      <h5>{trans("details")}</h5>
      <div className="user__products-wrapper table-responsive">
        <Table
          striped
          hover
          className="user-products__table table table-hover table-striped text-nowrap text-center"
        >
          <thead>
            <tr>
              <th>{trans("lisence_key")}</th>
              <th className="text-left w-25">
                {trans("product.title").toString()}
              </th>
              <th>{trans("account_number")}</th>
              <th>{trans("lisence_days")}</th>
              <th>{trans("created_at")}</th>
              <th>{trans("expired_at")}</th>
              <th>{trans("actions")}</th>
            </tr>
          </thead>
          <tbody>
            {!!paginate?.data?.length &&
              paginate.data.map((userLicense) => (
                <tr key={userLicense.id}>
                  <td>
                    {protectLicenseKey(userLicense.license_key)}
                    <i
                      className="fas fa-copy ml-2 cursor-pointer"
                      title={`${trans("copied")}`}
                      onClick={() => {
                        navigator.clipboard.writeText(userLicense.license_key);
                        toast.dismiss();
                        toast.success(trans("copied"));
                      }}
                    ></i>
                  </td>
                  <td className="text-left">
                    <ProductColComponent product={userLicense?.product} />
                  </td>
                  <td>
                    {userLicense?.account_number < 0
                      ? trans("unlimited")
                      : formatNumber(userLicense?.account_number)}
                  </td>
                  <td>
                    {userLicense?.license_days < 0
                      ? trans("unlimited")
                      : moment
                          .duration(userLicense?.license_days, "days")
                          .format()}
                  </td>
                  <td>{formatDate(userLicense?.created_at, "L")}</td>
                  <td>
                    {userLicense?.license_days < 0
                      ? trans("unlimited")
                      : userLicense?.expired_at > 0
                      ? formatDate(userLicense.created_at)
                      : "N/A"}
                  </td>
                  <td className="text-right">
                    <UserProductDropDownComponent userLicense={userLicense} />
                  </td>
                </tr>
              ))}
          </tbody>
        </Table>
      </div>

      <Pagination params={paginate} />
    </div>
  );
};

export default UserProductComponent;
