import React from "react";
import { IUser } from "@app/types";
import {
  UserProfileEmailComponent,
  UserProfileNameComponent,
} from "./UserProfileField";
import { useHeader, useTrans } from "@app/hooks";

const UserProfileComponent = ({ visitor }: { visitor: IUser }) => {
  const trans = useTrans();

  useHeader({
    pageTitle: trans("x_profile", { user_name: visitor.name }).toString(),
    breadcrumbs: [
      {
        title: trans("x_profile", { user_name: visitor.name }),
        active: true,
      },
    ],
  });

  return (
    <div className="user__profile">
      <div className="user__field">
        <div className="user__field-wrapper">
          <UserProfileNameComponent visitor={visitor} />
          <UserProfileEmailComponent visitor={visitor} />
        </div>
      </div>
    </div>
  );
};

export default UserProfileComponent;
