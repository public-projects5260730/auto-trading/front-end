import React, { useState } from "react";
import { useTrans } from "@app/hooks";
import { FormProvider, useForm } from "react-hook-form";
import { IUser } from "@app/types";
import { userApi } from "@app/apis";
import Form from "@app/components/Form";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faXmark } from "@fortawesome/free-solid-svg-icons";

const UserProfileNameComponent = ({ visitor }: { visitor: IUser }) => {
  const trans = useTrans();
  const methods = useForm<{ name: string }>({
    defaultValues: {
      name: visitor.name,
    },
  });
  const { handleSubmit, setError, reset } = methods;
  const [isChange, setIsChange] = useState(true);
  const [saveError, setSaveError] = useState("");

  const onSubmit = handleSubmit(({ name }: { name: string }) => {
    if (isChange) {
      return;
    }

    userApi
      .changeUserName(name)
      .then(({ successfully, user, error, errors }) => {
        if (error) {
          setSaveError(error);
        }

        if (errors) {
          Object.entries(errors).map(([field, message]) => {
            return setError(field as keyof { name: string }, {
              type: "manual",
              message,
            });
          });
        }

        if (successfully) {
          reset({ name: user.name });
          setIsChange(!isChange);
        }
      });
  });

  return (
    <FormProvider {...methods}>
      <form onSubmit={onSubmit} className="form-group">
        <div className="user__field-wrapper">
          {saveError?.length > 0 && (
            <div className="row">
              <div className="col-lg-3 col-sm-2"></div>
              <div className="col-lg-9 col-sm-10">
                <div className="user-profile__error">{saveError}</div>
              </div>
            </div>
          )}

          <div className="row">
            <div className="col-lg-3 col-sm-2 user-text-end">
              <label
                className="user-profile__text"
                htmlFor="user-profile__info-name"
              >
                {trans("name")}
              </label>
            </div>

            <div className="col-lg-9 col-sm-10">
              <div className="user-profile__info">
                <div className="mobile">
                  {isChange ? (
                    <button
                      className="user-profile__info-change-save edit"
                      onClick={() => {
                        setIsChange(!isChange);
                        reset({ name: "" });
                      }}
                    >
                      {trans("edit")}
                    </button>
                  ) : (
                    <div className="user-profile__info-custom">
                      <Form.Button
                        isSubmitting={isChange}
                        label={trans("save")}
                        className="user-profile__info-change-save"
                      />
                      <FontAwesomeIcon
                        className="user-profile__info-icon"
                        icon={faXmark}
                        onClick={() => {
                          setIsChange(!isChange);
                          reset({ name: visitor.name });
                        }}
                      />
                    </div>
                  )}
                </div>

                <div className="col-sm-11">
                  <Form.InputGroup name="name">
                    <Form.InputBox
                      name="name"
                      isRequired
                      disabled={isChange}
                      id="user-profile__info-name"
                    />
                  </Form.InputGroup>
                </div>

                <div className="col-sm-1 pc">
                  {isChange ? (
                    <button
                      className="user-profile__info-change-save"
                      onClick={() => {
                        setIsChange(!isChange);
                        reset({ name: "" });
                      }}
                    >
                      {trans("edit")}
                    </button>
                  ) : (
                    <div className="user-profile__info-custom">
                      <Form.Button
                        isSubmitting={isChange}
                        label={trans("save")}
                        className="user-profile__info-change-save"
                      />
                      <FontAwesomeIcon
                        className="user-profile__info-icon"
                        icon={faXmark}
                        onClick={() => {
                          setIsChange(!isChange);
                          reset({ name: visitor.name });
                        }}
                      />
                    </div>
                  )}
                </div>
              </div>
            </div>
          </div>
        </div>
      </form>
    </FormProvider>
  );
};

export default UserProfileNameComponent;
