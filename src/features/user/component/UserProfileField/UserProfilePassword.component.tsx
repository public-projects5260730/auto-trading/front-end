import React, { useState } from "react";
import { useHeader, useTrans } from "@app/hooks";
import { FormProvider, useForm } from "react-hook-form";
import { IChangePasswordData } from "@app/types";
import { userApi } from "@app/apis";
import Form from "@app/components/Form";
import { toast } from "react-toastify";

const UserProfilePasswordComponent = () => {
  const trans = useTrans();
  const methods = useForm<IChangePasswordData>();
  const { handleSubmit, setError } = methods;
  const [isSubmitting, setSubmitting] = useState(false);
  const [saveError, setSaveError] = useState("");

  useHeader({
    pageTitle: trans("change_password").toString(),
    breadcrumbs: [
      {
        title: trans("change_password"),
        active: true,
      },
    ],
  });

  const onSubmit = handleSubmit((changeData: IChangePasswordData) => {
    if (isSubmitting) {
      return;
    }
    setSubmitting(true);

    userApi
      .changeUserPassword(changeData)
      .then(({ successfully, error, errors }) => {
        setSubmitting(false);
        if (error) {
          setSaveError(error);
        }

        if (errors) {
          Object.entries(errors).map(([field, message]) => {
            return setError(field as keyof IChangePasswordData, {
              type: "manual",
              message,
            });
          });
        }

        if (successfully) {
          toast.dismiss();
          toast.success(trans("your_password_has_been_changed_successfully"));
        }
      });
  });

  return (
    <div className="user__field">
      <FormProvider {...methods}>
        <form onSubmit={onSubmit} className="form-group">
          <div className="user__field-wrapper">
            {saveError?.length > 0 && (
              <div className="row">
                <div className="col-lg-4 col-md-4 col-sm-2"></div>
                <div className="col-lg-8 col-md-8 col-sm-10">
                  <div className="user-profile__error">{saveError}</div>
                </div>
              </div>
            )}

            <div className="row">
              <div className="col-lg-4 col-md-4 col-sm-3 user-text-end">
                <label
                  className="user-profile__text"
                  htmlFor="user-profile__info-old-password"
                >
                  {trans("password")}
                </label>
              </div>
              <div className="col-lg-8 col-md-8 col-sm-9">
                <div className="user-profile__info">
                  <Form.InputGroup name="old_password">
                    <Form.InputBox
                      name="old_password"
                      type="password"
                      isRequired
                      disabled={isSubmitting}
                      id="user-profile__info-old-password"
                    />
                  </Form.InputGroup>
                </div>
              </div>
            </div>

            <div className="row">
              <div className="col-lg-4 col-md-4 col-sm-3 user-text-end">
                <label
                  className="user-profile__text"
                  htmlFor="user-profile__info-password"
                >
                  {trans("new_password")}
                </label>
              </div>
              <div className="col-lg-8 col-md-8 col-sm-9">
                <div className="user-profile__info">
                  <Form.InputGroup name="password">
                    <Form.InputBox
                      name="password"
                      type="password"
                      isRequired
                      disabled={isSubmitting}
                      id="user-profile__info-password"
                    />
                  </Form.InputGroup>
                </div>
              </div>
            </div>

            <div className="row">
              <div className="col-lg-4 col-md-4 col-sm-3 user-text-end">
                <label
                  className="user-profile__text"
                  htmlFor="user-profile__info-password-confirmation"
                >
                  {trans("password_confirmation")}
                </label>
              </div>
              <div className="col-lg-8 col-md-8 col-sm-9">
                <div className="user-profile__info">
                  <Form.InputGroup name="password_confirmation">
                    <Form.InputBox
                      name="password_confirmation"
                      type="password"
                      isRequired
                      disabled={isSubmitting}
                      id="user-profile__info-password-confirmation"
                    />
                  </Form.InputGroup>
                </div>
              </div>
            </div>
          </div>

          <Form.Button
            isSubmitting={isSubmitting}
            label={trans("save")}
            className="user-profile__info-btn save"
          />
        </form>
      </FormProvider>
    </div>
  );
};

export default UserProfilePasswordComponent;
