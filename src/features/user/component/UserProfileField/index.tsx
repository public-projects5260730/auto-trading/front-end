export { default as UserProfileNameComponent } from "./UserProfileName.component";
export { default as UserProfileEmailComponent } from "./UserProfileEmail.component";
export { default as UserProfilePasswordComponent } from "./UserProfilePassword.component";
