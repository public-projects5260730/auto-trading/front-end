import { pageApi } from "@app/apis";
import {
  LoadingComponent,
  MainTopComponent,
  NoDataComponent,
} from "@app/components";
import { useHeader, useLanguage, useTrans } from "@app/hooks";
import { IPage } from "@app/types";
import { raw } from "@app/utils";
import { useEffect, useState, useMemo } from "react";
import { from } from "rxjs";

const PageView = ({ slug }: { slug: string }) => {
  const trans = useTrans();
  const [page, setPage] = useState<IPage | null>();

  useHeader({
    pageTitle: trans(slug).toString(),
    breadcrumbs: [
      {
        title: trans(slug),
        active: true,
      },
    ],
  });

  useEffect(() => {
    const subscription = from(pageApi.getPageView(slug)).subscribe(
      ({ page }) => {
        setPage(page || null);
      }
    );

    return () => {
      subscription.unsubscribe();
    };
  }, [slug]);

  const productLanguage = useLanguage<IPage>(page);

  return (
    <div>
      <MainTopComponent />
      <div className="container mt-5 mb-5">
        {page === undefined && <LoadingComponent />}
        {page === null && <NoDataComponent />}
        {!!page?.id && (
          <div id="raw-html">{raw(productLanguage?.body || "")}</div>
        )}
      </div>
    </div>
  );
};

export default PageView;
