#!/usr/bin/env bash
mkdir ./build
# sh ./bin/scripts/app-config.sh
sh ./bin/scripts/app-config.dist.sh

# cp ./public/assets/app-config.js ./build/assets/app-config.js
cp ./public/assets/app-config.js.dist ./build/app-config.js.dist

mv .env .env.build

craco build

mv .env.build .env

rm -rf C:/xampp/htdocs/works/AutoTrading/back-end/public/static
cp -r ./build/* C:/xampp/htdocs/works/AutoTrading/back-end/public