#!/bin/bash

# Recreate config file
rm -rf ./public/assets/app-config.js.dist
touch ./public/assets/app-config.js.dist

# Add assignment 
echo "window._env_ = {" >> ./public/assets/app-config.js.dist

# Read each line in .env file
# Each line represents key=value pairs
while read -r line || [[ -n "$line" ]];
do

  # Ignore comment line
  if [[ $line =~ ^\s*\# ]]; then
    continue
  fi

  # get name and value
  if [[ $line =~ ^\s*([a-z_]+)\s*\=\s*([\'\"])?([^\'\"]*)($2|$) ]]; then
    varname=$(printf '%s\n' "${BASH_REMATCH[1]}")
    varvalue=$(printf '%s\n' "")
  fi

  # Split env variables by character `=`
  # if printf '%s\n' "$line" | grep -q -e '='; then
  #   varname=$(printf '%s\n' "$line" | sed -e 's/=.*//')
  #   varvalue=$(printf '%s\n' "$line" | sed -e 's/^[^=]*=//')

  #   echo $varname;
  #   echo $varvalue;
  # fi

  # Read value of current variable if exists as Environment variable
  value=$(printf '%s\n' "${!varname}")
  # Otherwise use value from .env file
  [[ -z $value ]] && value=${varvalue}
  
  # Append configuration property to JS file
  echo "  $varname: \"$value\"," >> ./public/assets/app-config.js.dist
done < .env

echo "}" >> ./public/assets/app-config.js.dist